# proteomics_tools

This is a set of scripts that are useful for analyzing and visualizing
proteomics data.

## Contributors

- Austin Keller
- Jarrett Egertson

## Contact

For more information send an email to <atkeller@uw.edu>.