"""
Written by Austin Keller <atkeller@u.washington.edu>
MacCoss Lab
University of Washington, Dept. of Genome Sciences
2016-10-26
"""

import argparse
import logging
import os.path
import re
import sqlite3

import pandas as pd

from .uniprot import ProteinId

# Skyline fix functions from Jarrett's Elibtools ElibBoundaries.py. Elibtools isn't setup as a module so importing isn't
# an option. We've copied the functionality that we need below.
mod_regex = re.compile("\[\+\d+\.\d+\]")


def transform_mod(x):
    """writes out a new modification string for an old regex string matched by other regex"""
    return "[+%d]" % (round(float(x.group()[2:-1])))


def transform_pep_mod_seq(s):
    """
    Skyline peak boundaries files expect peptide modifications to be written as integer (e.g. +57) and
     no asterisk notation , this function transforms to a skyline compatible sequence
    :param s: petpide mod seq
    :return: compatible sequence
    """
    mod_fixed = mod_regex.sub(transform_mod, s)
    if mod_fixed[0] == '*':
        mod_fixed = mod_fixed[1:]
    if mod_fixed[-1] == '*':
        mod_fixed = mod_fixed[:-1]
    return mod_fixed
## End Elibtools functions


class ElibReader(object):
    """
    The .elib format is a sqlite3 database. This class provides convenience functions for extracting data and performing
    common transformations.
    """
    def __init__(self, filename=None, table='proteins', selection='*'):
        self.filename = filename
        self.table = table
        self.selection = selection
        self.generator_function = {
            'proteins': self.parse_protein_table,
            'entries': self.parse_table,
            'peptidequants': self.parse_table,
            'fragmentquants': self.parse_table,
            'metadata': self.parse_table
        }

    def read(self, filename):
        self.filename = filename
        # TODO validate file format

    def set_table(self, table):
        self.table = table

    def set_selection(self, selection):
        self.selection = selection

    def __iter__(self):
        # elib is a sqlite3 database
        if not os.path.isfile(self.filename):
            raise Exception("File {} does not exist".format(self.filename))
        conn = sqlite3.connect(self.filename)
        cursor = conn.cursor()
        for result in self.generator_function[self.table](cursor):
            yield result

    def parse_protein_table(self, cursor):
        """

        :type cursor: sqlite3.Cursor
        """
        # Write the peptides from the elib
        id_parser = ProteinIdParser()
        for row in cursor.execute("SELECT {selection} FROM proteins".format(selection=self.selection)):
            sequence = row[0]
            protein_id = row[1]
            id_parser.parse(protein_id)
            yield id_parser.unique_ids[0], sequence  # HACK Just give first entry losing other proteins with this sequence

    def parse_table(self, cursor):
        """

        :type cursor: sqlite3.Cursor
        """
        cursor.execute("SELECT {selection} FROM entries".format(selection=self.selection))
        column_names = []
        for item in cursor.description:
            column_names.append(item[0])
        for row in cursor:
            yield dict(zip(column_names, row))

    def get_dataframe(self, skyline_fix=False):
        conn = sqlite3.connect(self.filename)
        df = pd.read_sql_query("SELECT {selection} FROM {table}".format(selection=self.selection, table=self.table), conn)
        if skyline_fix:
            if self.table == "entries":
                self._apply_skyline_compatibility(df)
            else:
                logging.warning("Skyline fix applied but has no effect")
        return df

    @staticmethod
    def _apply_skyline_compatibility(df):
        df.loc[:, "PeptideModSeq"] = df['PeptideModSeq'].apply(transform_pep_mod_seq)

    def write_peptide_list(self, outfile_name=""):
        if not outfile_name:
            outfile_name = "".join(self.filename.split('.')[:-1]) + "_peptides.txt"

        # Open the output file
        outfile = open(outfile_name, 'w')

        print("Writing to {}".format(outfile_name))

        # Write the header
        outfile.write("Protein_Name\tSequence\n")

        # Write the peptides from the elib
        id_parser = ProteinIdParser()
        for id, sequence in self:
            outfile.write("{0}\t{1}\n".format(id, sequence))


class ProteinIdParser(object):
    """
    :type protein_ids: list[ProteinId]
    :type original_id: ProteinId
    """
    def __init__(self):
        self.protein_ids = []
        self.unique_ids = []

    def parse(self, protein_id_string):
        """
        Read semi-colon separated protein ids in elib and consolidate the multiple unique ids into a single "original"
        id corresponding to the sequence
        :param string protein_id_string:
        """
        # clear
        self.__init__()

        # The elib has multiple accessions ids for each peptide sequence
        protein_id_string_set = protein_id_string.split(';')
        for protein_id in protein_id_string_set:
            self.protein_ids.append(ProteinId(protein_id))

        # For each unique protein make one entry
        proteins = set()
        for id in self.protein_ids:
            db, unique_identifier, entry_name = tuple(id)
            if entry_name not in proteins:
                proteins.add(entry_name)
                self.unique_ids.append(id)


if __name__ == '__main__':

    # Get args
    parser = argparse.ArgumentParser()
    parser.add_argument("inputfile")
    parser.add_argument("-o", "--outputfile")
    args = parser.parse_args()

    inputfile = args.inputfile
    print(inputfile)
    if args.outputfile is not None:
        outputfile = args.outputfile
    else:
        outputfile = '.'.join(inputfile.split('.')[:-1]) + "_peptides.txt"

    reader = ElibReader()
    reader.read(inputfile)
    reader.write_peptide_list(outputfile)
