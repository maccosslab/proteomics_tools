"""
Written by Austin Keller <atkeller@u.washington.edu>
MacCoss Lab
University of Washington, Dept. of Genome Sciences
2016-10-28
"""


if __name__ == '__main__':
    import argparse
    from glob import glob
    import pandas as pd
    import numpy as np
    from .elib_reader import ElibReader
    from .peptide_list_reader import PeptideListEntry, PeptideSequenceEntry

    # Allow for unix-style argument list of files
    parser = argparse.ArgumentParser(description='')
    parser.add_argument('files', nargs='*')
    parser.add_argument('-e', '--elib')
    parser.add_argument('-o', '--outfile')
    args = parser.parse_args()

    #DEBUG
    #print(args.files)
    #print(args.elib)

    files = []
    for entry in args.files:
        files.extend(glob(entry))

    # Read elib
    print("Reading {}".format(args.elib))
    elib_reader = ElibReader()
    elib_reader.read(args.elib)
    unique_entries = set()
    for id, sequence in elib_reader:
        entry = PeptideListEntry()
        entry.protein_id = id
        entry.sequence = sequence
        unique_entries.add(PeptideSequenceEntry(entry))

    # Pull out the sequences and make them follow the format used by Pecan
    unique_sequences = set(('-.' + entry.sequence + '.-' for entry in unique_entries))

    # Get unique sequences
    files = list(set(files))
    files.sort()

    for feat_file in files:
        # Extract target features. This takes the intersection of the elib and feature-file sequences
        print("Extracting targets from {}".format(feat_file))
        df = pd.read_csv(feat_file, sep='\t')
        targets = df[df.loc[:, 'TD'] == 1]
        extracted_targets = targets[[seq in unique_sequences for seq in targets.loc[:, 'sequence']]]

        # Randomly sample decoys
        decoys = df[df.loc[:, 'TD'] == -1]
        num_decoys = len(decoys.iloc[:, ])
        num_ext_targets = len(extracted_targets.iloc[:, ])
        np.random.seed(0)
        selection = np.random.choice(num_decoys, num_ext_targets, replace=False)
        decoys = decoys.iloc[selection]
        num_decoys = len(decoys.iloc[:, ])
        new_features = pd.concat([extracted_targets, decoys])
        """:type: pandas.DataFrame"""
        num_new_features = len(new_features.iloc[:, ])

        #DEBUG
        print("Num new features: {}".format(num_new_features))
        print("Num ext targets: {}".format(num_ext_targets))
        print("Num decoys: {}".format(num_decoys))

        assert(num_new_features == num_ext_targets + num_decoys)

        # Write results
        out_file = ".".join(feat_file.split('.')[:-2])
        out_file += "_detectable.pecan.feature"
        print("Writing to {}".format(out_file))
        new_features.to_csv(out_file, sep='\t', index=False)



