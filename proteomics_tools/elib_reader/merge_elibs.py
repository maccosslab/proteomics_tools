"""
Written by Austin Keller <atkeller@u.washington.edu>
MacCoss Lab
University of Washington, Dept. of Genome Sciences
2016-10-26
"""

import argparse
from glob import glob

from .elib_reader import ElibReader
from .peptide_list_reader import PeptideListReader, PeptideSequenceEntry

# Allow for unix-style argument list of files
parser = argparse.ArgumentParser(description='Merge elibs into a single peptide list')
parser.add_argument('files', nargs='*')
parser.add_argument('-o', '--outfile')
args = parser.parse_args()
files = []
for entry in args.files:
    files.extend(glob(entry))

# Get unique
files = list(set(files))

# Make pretty
files.sort()

peptide_list_filenames = []

# Convert elibs to peptide lists
for file in files:
    elib_reader = ElibReader()
    elib_reader.read(file)
    outputfile = '.'.join(file.split('.')[:-1]) + "_peptides.txt"
    peptide_list_filenames.append(outputfile)
    elib_reader.write_peptide_list(outfile_name=outputfile)

# Merge lists
peptide_entries = []
pep_reader = PeptideListReader()
for file in peptide_list_filenames:
    pep_reader.read(file)
    peptide_entries.extend(pep_reader.entries)


peptide_entries = list(set(PeptideSequenceEntry(entry) for entry in peptide_entries))
""":type : list[PeptideListEntry]"""

# Write main file
outfile = "merged_peptides.txt"
if args.outfile is not None:
    outfile = args.outfile

with open(outfile, 'w') as f:
    f.write("Protein_Name\tSequence\n")
    for entry in peptide_entries:
        f.write("{0}\t{1}\n".format(entry.protein_id, entry.sequence))