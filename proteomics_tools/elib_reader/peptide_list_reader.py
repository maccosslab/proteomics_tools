"""
Written by Austin Keller <atkeller@u.washington.edu>
MacCoss Lab
University of Washington, Dept. of Genome Sciences
2016-10-26
"""
from .uniprot import ProteinId
import hashlib


class PeptideListEntry(object):
    """
    type sequence: string
    type protein_id: ProteinId
    """

    def __init__(self):
        self.sequence = None
        self.protein_id = None

    def __eq__(self, other):
        return self.__hash__() == hash(other)

    def __hash__(self):
        return hash(self.sequence + str(self.protein_id))



class PeptideSequenceEntry(object):
    """
    Wrapper for peptide entry that only cares about sequence info
    """
    def __init__(self, peptide_list_entry):
        self.wrapped_class = peptide_list_entry

    def __getattr__(self, attr):
        """
        Generic pass-through function for accessing attributes of wrapped classes
        :param attr:
        :return:
        """
        orig_attr = self.wrapped_class.__getattribute__(attr)
        if callable(orig_attr):
            def hooked(*args, **kwargs):
                result = orig_attr(*args, **kwargs)
                # prevent wrapped_class from becoming unwrapped
                if result == self.wrapped_class:
                    return self
                return result
            return hooked
        else:
            return orig_attr

    def __eq__(self, other):
        return self.sequence == other.sequence

    def __hash__(self):
        return hash(self.sequence)


class PeptideListReader(object):
    def __init__(self):
        self.entries = []

    def read(self, filename):
        # clear
        self.__init__()

        f = open(filename, 'r')

        # Read header
        header = f.readline().rstrip('\n').split('\t')
        header = [x.lower() for x in header]
        try:
            protein_name_idx = header.index('protein_name')
            sequence_index = header.index('sequence')
        except ValueError:
            print("ValueError: Header not as expected in {0}. Header observed is {1}".format(filename, header))
            raise

        # Read entries
        for line in f.readlines():
            row_vals = line.rstrip('\n').split('\t')
            protein_id = row_vals[protein_name_idx]
            sequence = row_vals[sequence_index]
            entry = PeptideListEntry()
            entry.sequence = sequence
            entry.protein_id = ProteinId(protein_id)
            self.entries.append(entry)