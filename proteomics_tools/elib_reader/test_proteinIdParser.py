from unittest import TestCase

from .elib_reader import ProteinIdParser

protein_accessions = [
    "sp|O43390|HNRPR_HUMAN",
    "sp|O43390-2|HNRPR_HUMAN",
    "sp|O43390-3|HNRPR_HUMAN",
    "sp|O43390-4|HNRPR_HUMAN",
    "sp|P02765|FETUA_HUMAN",
    "sp|P78527|PRKDC_HUMAN",
    "sp|P78527-2|PRKDC_HUMAN",
]


class TestProteinIdParser(TestCase):

    def setUp(self):
        self.parser = ProteinIdParser()
        self.parser.parse(";".join(protein_accessions))

    def test_parse_all_ids(self):
        self.assertEqual(len(self.parser.protein_ids), len(protein_accessions))

    def test_parse_unique_ids(self):
        self.assertEqual(len(self.parser.unique_ids), 3)
