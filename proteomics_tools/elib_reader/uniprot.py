"""
Written by Austin Keller <atkeller@u.washington.edu>
MacCoss Lab
University of Washington, Dept. of Genome Sciences
2016-10-26
"""


class ProteinId(object):
    def __init__(self, id_string=None):
        self.db = None
        self.unique_identifier = None
        self.entry_name = None
        if id_string is not None:
            self.db, self.unique_identifier, self.entry_name = id_string.split('|')

    @staticmethod
    def from_string(id_string):
        return ProteinId(id_string)

    def __str__(self):
        return "{0}|{1}|{2}".format(self.db, self.unique_identifier, self.entry_name)

    def __iter__(self):
        """
        Make iterable for easy conversion to tuple
        """
        yield self.db
        yield self.unique_identifier
        yield self.entry_name

    def to_tuple(self):
        return tuple(self)
