"""
File readers copying MSToolkit functionality
"""

import os
import struct
import array
import zlib
import sys
import copy
from bisect import bisect_right
import threading
import pymzml
import time
from ..helpers.helpers import handle_possible_windows_shortcut


# general threading class
class ReaderThread(threading.Thread):
    def __init__(self, parent_reader, state_event_handler=None, filename=None):
        threading.Thread.__init__(self)
        '''state_event_handler is a ReaderEventMapper object to which reader status updates are sent'''
        self.state_event_handler = state_event_handler
        self.parent_reader = parent_reader
        self.filename = filename
        self.setDaemon(True)

    def run(self):
        if self._has_event_handler():
            self.state_event_handler.process_event("loading")
        self.parent_reader.state = "loading"
        # load up the file
        self.parent_reader.load_file(self.filename)
        if self._has_event_handler():
            self.state_event_handler.process_event("loaded")
        self.parent_reader.state = "loaded"

    def set_filename(self, filename):
        self.filename = filename

    def _has_event_handler(self):
        return not (self.state_event_handler is None)


# MS1 file support

class Spectrum:
    def __init__(self, num_peaks=0, peaks=None, annotations=None):
        if annotations is None:
            annotations = list()
        if peaks is None:
            peaks = list()
        self.scan_info = MSScanInfo()
        # MSScanInfo has some redundnacies with variables below MSScanInfo is filled when the spectrum is read by the ms2 file reader
        self.num_peaks = num_peaks
        self.peaks = list()
        self.annotations = list()
        self.scan = 0
        self.RTime = 0
        self.BPI = 0
        self.BPM = 0
        self.TIC = 0
        self.IIT = 0
        self.is_sorted = False
        self.z_states = list()

    def add_peak(self, mz, intensity):
        self.peaks.append((mz, intensity))
        self.annotations.append(None)
        self.num_peaks += 1
        self.is_sorted = False

    def near(self, mz):
        if not self.is_sorted:
            self.peaks.sort()
        self.is_sorted = True
        index = bisect_right(self.peaks, (mz, 0))
        if index == 0:
            return self.peaks[0]
        elif index == len(self.peaks):
            return self.peaks[index - 1]
        else:
            before_diff = abs(self.peaks[index - 1][0] - mz)
            after_diff = abs(self.peaks[index][0] - mz)
            if before_diff < after_diff:
                return self.peaks[index - 1]
            else:
                return self.peaks[index]

    def set_annotation(self, peak_num, color="Black", label=""):
        self.annotations[peak_num] = Annotation(color, label)

    def clear_annotations(self):
        self.annotations = list()
        for i in range(len(self.peaks)):
            self.annotations.append(None)


class MSScanInfo:
    def __init__(self):
        self.iVersion = -1
        self.scanNumber = -1
        self.scanNumber2 = -1
        self.mz = -1
        self.rTime = -1
        self.bpi = -1
        self.bpm = -1
        self.convA = -1
        self.convB = -1
        self.tic = -1  # total ion current
        self.iit = -1  # ion injection time
        self.numZStates = 0
        self.numEZStates = 0
        self.numDataPoints = -1
        self.isolation_width = -1
        self.isolation_center = -1
        self.low_mass = -1
        self.high_mass = -1


class Annotation:
    color = "Black"
    label = ""
    charge = 0
    ion_type = ""

    def __init__(self, color="Black", label="", charge=0, ion_type=""):
        self.color = color
        self.label = label
        self.charge = charge
        self.ion_type = ion_type


class MS1Reader:
    def __init__(self, filename=""):
        self.filename = filename
        self.spectra_indices = dict()
        self.state = "initialized"

    def load_file(self, filename=None):
        if filename is None:
            filename = self.filename
        else:
            self.filename = filename
        fin = open(filename)
        pre_pos = 0
        while True:
            pre_pos = fin.tell()
            line = fin.readline()
            if line == '':
                # EOF
                break
            if line[0] == 'S':
                values = line.strip().split('\t')
                scan = int(values[1])
                self.spectra_indices[scan] = pre_pos
        fin.close()

    def at(self, scan):
        if not self.spectra_indices.has_key(scan):
            print("Scan " + str(scan) + " does not exist in this file!")
            return Spectrum()
        index = self.spectra_indices[scan]
        return self.get_spectrum_from_index(index)

    def get_spectrum_from_index(self, index):
        fin = open(self.filename)
        fin.seek(index, 0)
        ret_spec = Spectrum()
        scan_line = fin.readline()
        if scan_line[0] != 'S':
            print("Error reading spectrum!")
            return
        ret_spec.scan = int(scan_line.split('\t')[1])
        for line in fin:
            if line[0] == 'I':
                values = line.strip().split('\t')
                param_name = values[1]
                param_val = float(values[2])
                setattr(ret_spec, param_name, param_val)
            elif line[0].isdigit():
                values = line.strip().split(' ')
                mz = float(values[0])
                intensity = float(values[1])
                ret_spec.add_peak(mz, intensity)
            elif line[0] == 'S':
                return ret_spec
        return ret_spec

    def get_scans(self):
        return self.spectra_indices.keys()


# sqt file support (percolator and non-percolator)
class Sequest_hit:

    def __init__(self, low_scan=0, high_scan=0, matches=None, is_perc=False):
        self.low_scan = low_scan
        self.high_scan = high_scan
        if matches is not None:
            self.matches = matches
        else:
            self.matches = []
        self.is_perc = False
        self.charges = set()

    # public functions
    def add_match(self, match):
        self.matches.append(match)
        self.charges.add(match.charge)

    def get_top_match(self, charge=None, use_evalue=False):
        """
        Returns the best match for this scan,

        :param charge: If charge is passed, it will return the best match for that charge
        :param use_evalue:
        :return:
        """
        best_match = Sequest_match()
        if len(self.matches) == 0:
            print("Tried to get top match from a list of no matches!")
        if charge is None:
            if use_evalue:
                # in a SEQUEST file, the e-value is put in the Sp value column
                best_evalue= self.matches[0].Sp
                best_match = self.matches[0]
                for match in self.matches:
                    if match.Sp < best_evalue:
                        best_evalue = match.Sp
                        best_match = match
            elif hasattr(self.matches[0], "q_val"):
                best_qval = 1.0
                best_match = self.matches[0]
                for match in self.matches:
                    if match.q_val < best_qval:
                        best_qval = match.q_val
                        best_match = match
            else:
                best_xcorr = 0
                best_match = self.matches[0]
                for match in self.matches:
                    if match.Xcorr > best_xcorr:
                        best_xcorr = match.Xcorr
                        best_match = match
        else:
            if charge not in self.charges:
                sys.stderr.write("Tried to get top match from a Sequest_hit for a non-existent charge!\n")
                return Sequest_match()
            if use_evalue:
                possible_matches = [match for match in self.matches if match.charge == charge]
                best_evalue = possible_matches[0].Sp
                best_match = possible_matches[0]
                for match in possible_matches:
                    if match.Sp < best_evalue:
                        best_evalue = match.Sp
                        best_match = match
            elif hasattr(self.matches[0], "q_val"):
                best_qval = 1.0
                for match in self.matches:
                    if match.charge == charge and match.q_val < best_qval:
                        best_qval = match.q_val
                        best_match = match
            else:
                best_xcorr = 0
                for match in self.matches:
                    if match.charge == charge and match.Xcorr > best_xcorr:
                        best_xcorr = match.Xcorr
                        best_match = match
        return best_match


class Sequest_match:
    Xcorr_rank = 0
    Sp_rank = 0
    calc_mass = 0  # MH+
    deltCN = 0
    Xcorr = 0
    Sp = 0
    matched_ions = 0
    expected_ions = 0
    sequence = ""
    validation_status = ""
    locii = list()

    obs_mass = 0  # MH+
    intensity = 0
    lowest_sp = 0
    charge = 0
    num_sequences_match = 0

    def __init__(self, Xcorr_rank=0, Sp_rank=0, calc_mass=0, deltCN=0, Xcorr=0, Sp=0, matched_ions=0, expected_ions=0,
                 sequence="", validation_status="", locii=None, obs_mass=0, intensity=0, lowest_sp=0, charge=0,
                 num_sequences_match=0):
        self.Xcorr_rank = Xcorr_rank
        self.Sp_rank = Sp_rank
        self.calc_mass = calc_mass
        self.deltCN = deltCN
        self.Xcorr = Xcorr
        self.Sp = Sp
        self.matched_ions = matched_ions
        self.expected_ions = expected_ions
        self.sequence = sequence
        self.validation_status = validation_status
        self.locii = list()

        self.obs_mass = obs_mass
        self.intensity = intensity
        self.lowest_sp = lowest_sp
        self.charge = 0
        self.num_sequences_match = 0


class Sequest_match_perc(Sequest_match):
    perc_score = 0
    q_val = 0

    def __init__(self, perc_score=0, q_val=0):
        Sequest_match.__init__(self)
        self.perc_score = perc_score
        self.q_val = q_val


class SQTIter:
    """an iterator for accessing the data in an SQT file sequentially
    faster and more memory efficient than using the .at function of SQTFileReader
    to use the iterator do
    for sqt_hit in SQTFileReader("myfile.sqt")"""

    def __init__(self):
        pass

    def next(self):
        pass

    def __iter__(self):
        return self


class SQTFileReader:
    # initalization
    def __init__(self, filename="", is_perc=False):
        self.filename = filename
        self.is_perc = is_perc
        self.state = "initialized"
        self.spectra_indices = dict()

    # public functions
    def load_file(self, filename=None):
        self.state = "loading"
        if filename is None:
            filename = self.filename
        else:
            self.filename = filename
        self.process_header()
        self.populate_indices()
        self.state = "loaded"

    # helper functions
    def process_header(self):
        """
        Open the file, read in the header information, and detect if this is a
        file that has been processed by percolator
        """
        fin = open(self.filename, 'rt')
        for line in fin:
            if line[0] != 'H':
                break
            if "percolator" in line:
                self.is_perc = True
        fin.close()

    def populate_indices(self):
        fin = open(self.filename, 'rb')
        pre_pos = 0
        # clear out indices that may have been present from a previous file
        self.spectra_indices = dict()
        low_scan = 0
        high_scan = 0
        while True:
            pre_pos = fin.tell()
            line = fin.readline()

            # DEBUG
            # fin.seek(pre_pos)
            # test_line = fin.readline()
            #
            # if test_line != line:
            #     raise Exception("Sequest file read offset is not behaving properly")

            if line == '':
                # EOF
                break
            if line[0] == 'S':
                vals = line.split('\t')
                t_low = int(vals[1])
                t_high = int(vals[2])
                if not (t_low == low_scan and t_high == high_scan):
                    # haven't seen this scan before
                    low_scan = t_low
                    high_scan = t_high
                    self.spectra_indices[low_scan] = pre_pos
        fin.close()

    def read_sequest_hit(self, index):
        fin = open(self.filename, 'rb')
        fin.seek(index, 0)
        found_scan = False
        low_scan = 0
        high_scan = 0
        # store the lines from the file that make up this SequestHit
        data_lines = list()
        line = fin.readline()
        line_vals = line.split('\t')
        low_scan = int(line_vals[1])
        high_scan = int(line_vals[2])
        data_lines.append(line)
        while True:
            line = fin.readline()
            if line == '':
                # at the end of the file
                break
            elif line[0] == 'S':
                t_low = int(line.split('\t')[1])
                t_high = int(line.split('\t')[2])
                if t_low == low_scan and t_high == high_scan:
                    # this is another match for this scan line, just a different charge
                    data_lines.append(line)
                else:
                    break
            else:
                data_lines.append(line)

        # now data_lines has been properly updated

        # process the data_lines
        # set the scan data
        seq_hit = Sequest_hit()
        seq_hit.is_perc = self.is_perc
        scan_line = data_lines[0]
        values = scan_line.strip().split('\t')
        seq_hit.low_scan = int(values[1])
        seq_hit.high_scan = int(values[2])
        charge = int(values[3])
        obs_mass = float(values[6])
        intensity = float(values[7])
        lowest_sp = float(values[8])
        num_sequences_match = int(values[9])
        if self.is_perc:
            seq_match = Sequest_match_perc()
        else:
            seq_match = Sequest_match()
        seq_match.charge = charge
        seq_match.obs_mass = obs_mass
        seq_match.intensity = intensity
        seq_match.lowest_sp = lowest_sp
        seq_match.num_sequences_match = num_sequences_match
        valid_match = False

        for i in range(1, len(data_lines)):
            line = data_lines[i]
            values = line.strip().split('\t')
            if line[0] == 'M':
                if valid_match:
                    seq_hit.add_match(seq_match)
                    if self.is_perc:
                        seq_match = Sequest_match_perc()
                    else:
                        seq_match = Sequest_match()
                    seq_match.charge = charge
                    seq_match.obs_mass = obs_mass
                    seq_match.intensity = intensity
                    seq_match.lowest_sp = lowest_sp
                    seq_match.num_sequences_match = num_sequences_match
                valid_match = True
                seq_match.Xcorr_rank = int(values[1])
                seq_match.Sp_rank = int(values[2])
                seq_match.calc_mass = float(values[3])
                seq_match.deltCN = float(values[4])
                if self.is_perc:
                    seq_match.perc_score = float(values[5])
                    seq_match.q_val = float(values[6]) * -1.0
                else:
                    seq_match.Xcorr = float(values[5])
                    seq_match.Sp = float(values[6])
                seq_match.matched_ions = int(values[7])
                seq_match.expected_ions = int(values[8])
                seq_match.sequence = values[9]
                seq_match.validation_status = values[10]
            elif line[0] == 'L':
                locus = values[1]
                description = ""
                if len(values) > 2:
                    description = values[2]
                seq_match.locii.append((locus, description))
            elif line[0] == 'S':
                if valid_match:
                    seq_hit.add_match(seq_match)
                    if self.is_perc:
                        seq_match = Sequest_match_perc()
                    else:
                        seq_match = Sequest_match()
                charge = int(values[3])
                obs_mass = float(values[6])
                intensity = float(values[7])
                lowest_sp = float(values[8])
                num_sequences_match = int(values[9])
                seq_match.charge = charge
                seq_match.obs_mass = obs_mass
                seq_match.intensity = intensity
                seq_match.lowest_sp = lowest_sp
                seq_match.num_sequences_match = num_sequences_match
                valid_match = False
        seq_hit.add_match(seq_match)
        return seq_hit

    def at(self, scan_num):
        if self.spectra_indices.has_key(scan_num):
            index = self.spectra_indices[scan_num]
            sqt_hit = self.read_sequest_hit(index)
            return True, sqt_hit
        else:
            sys.stderr.write("Tried to access a non-existent scan in an sqt file!")
            return False, None

    def valid_scans(self):
        ret = self.spectra_indices.keys()
        ret.sort()
        return ret


class TabDelimFileReader:
    """
    Emulates the functionality of SQTFileReader, but reads from Jimmy's tab delimited
    data format
    """

    def __init__(self, filename=""):
        self.filename = filename
        self.state = "initialized"
        self.scan_data = dict()

    def load_file(self, filename=None):
        self.state = "loading"
        if filename is None:
            filename = self.filename
        else:
            self.filename = filename
        fin = open(self.filename, 'rt')
        fin.readline()
        # populate self.scan_data with the data from the input file
        for line in fin:
            values = line.strip().split('\t')
            scan = int(values[0])
            charge = int(values[1])
            xcorr = float(values[2])
            sequence = values[3]
            if self.scan_data.has_key(scan):
                self.scan_data[scan].append((charge, xcorr, sequence))
            else:
                self.scan_data[scan] = [(charge, xcorr, sequence)]
        fin.close()

    def valid_scans(self):
        """return a list of scans read from the input file"""
        ret = self.scan_data.keys()
        ret.sort()
        return ret

    def at(self, scan_num):
        if self.scan_data.has_key(scan_num):
            sqt_hit = Sequest_hit()
            sqt_hit.low_scan = scan_num
            sqt_hit.high_scan = scan_num
            for match in self.scan_data[scan_num]:
                charge = match[0]
                xcorr = match[1]
                sequence = match[2]
                sqt_match = Sequest_match()
                sqt_match.Xcorr = xcorr
                sqt_match.charge = charge
                sqt_match.sequence = sequence
                sqt_hit.add_match(sqt_match)
            return True, sqt_hit
        else:
            sys.stderr.write("Tried to access a non-existent scan in the tab- delimited sqt file!")
            return False, None


class TandemPepTxtEntry(object):
    """Holds information about a particular XTandem hit: charge, hyperscore, expectation score, sequence, protein, RT"""

    def __init__(self, charge=None, hyperscore=None, evalue=None, sequence=None, protein=None, rt=None):
        self._charge = charge
        self._hyperscore = hyperscore
        self._evalue = evalue
        self._sequence = sequence
        self._protein = protein
        self._rt = rt

    @property
    def charge(self):
        return self._charge

    @charge.setter
    def charge(self, value):
        self._charge = value

    @property
    def hyperscore(self):
        return self._hyperscore

    @hyperscore.setter
    def hyperscore(self, value):
        self._hyperscore = value

    @property
    def evalue(self):
        return self._evalue

    @evalue.setter
    def evalue(self, value):
        self._evalue = value

    @property
    def sequence(self):
        return self._sequence

    @sequence.setter
    def sequence(self, value):
        self._sequence = value

    @property
    def protein(self):
        return self._protein

    @protein.setter
    def protein(self, value):
        self._protein = value

    @property
    def rt(self):
        return self._rt

    @rt.setter
    def rt(self, value):
        self._rt = value


class TandemPepTxtSpectrum(object):
    """Holds information for a spectrum from a Tandem pep.xml.txt files.  The spectrum is identified by a unique scan number
    but can have multiple TandemPepTxtEntry objects representing multiple matches for this scan"""

    def __init__(self, scan=None):
        self._scan = scan
        self._hits = list()
        self._charges = set()

    @property
    def scan(self):
        return self._scan

    @scan.setter
    def scan(self, value):
        self._scan = value

    @property
    def hits(self):
        return copy.deepcopy(self._hits)

    @property
    def charges(self):
        return copy.deepcopy(self._charges)

    def AddHit(self, hit):
        """Add a xtandem hit for this spectrum, hit is an object of type TandemPepTxtEntry"""
        self._hits.append(copy.deepcopy(hit))
        self._charges.add(hit.charge)

    def GetTopHit(self, score='EVAL', charge=None):
        if charge is not None:
            if charge not in self._charges:
                raise Exception("GetTopHit: queried charge does not exist\n")
            validHits = [entry for entry in self._hits if entry.charge == charge]
        else:
            validHits = self._hits

        best_hit = validHits[0]
        if score == 'EVAL':
            best_eval = validHits[0].evalue
            best_hit = validHits[0]
            for hit in validHits:
                if hit.evalue < best_eval:
                    best_eval = hit.evalue
                    best_hit = hit
        elif score == 'HYPERSCORE':
            best_score = validHits[0].hyperscore
            best_hit = validHits[0]
            for hit in validHits:
                if hit.hyperscore > best_score:
                    best_score = hit.hyperscore
                    best_hit = hit
        else:
            raise Exception("[generic.py]TandemPepTxSpectrum:GetTopHit: score must be 'EVAL' or 'HYPERSCORE'\n")
        return copy.deepcopy(best_hit)


class TandemPepTxtReader(object):
    """reads PSMs from a .pep.xml.txt files generated by taking XTandem output, running Tandem2XML, then pepxml2txt (by J Eng)"""

    def __init__(self, filename=""):
        self._filename = filename
        self._state = "intialized"
        # self._scan_data[scan number] -> TandemPepTxtSpectrum
        self._scan_data = dict()

    @property
    def filename(self):
        return self._filename

    @filename.setter
    def filename(self, value):
        self._filename = value

    @property
    def state(self):
        return self._state

    def load_file(self, filename=None):
        self._state = "loading"
        if filename is None:
            filename = self.filename
        else:
            self.filename = filename
        fin = open(self.filename, 'rt')
        fin.readline()
        for line in fin:
            values = line.strip().split('\t')
            scan = int(values[0].split('=')[-1])
            charge = int(values[1])
            hyperscore = float(values[2])
            evalue = float(values[3])
            sequence = values[4]
            protein = values[5]
            try:
                rt = float(values[7])
            except ValueError:
                rt = None

            hit = TandemPepTxtEntry(charge, hyperscore, evalue, sequence, protein, rt)
            if self._scan_data.has_key(scan):
                self._scan_data[scan].AddHit(hit)
            else:
                self._scan_data[scan] = TandemPepTxtSpectrum(scan)
                self._scan_data[scan].AddHit(hit)
        fin.close()
        self._state = "loaded"

    def at(self, scan):
        if not self._scan_data.has_key(scan):
            sys.stderr.write("\nTried to access a scan not present in the ms2 file\n")
            return False, None
        return True, copy.deepcopy(self._scan_data[scan])

    def get_valid_scans(self):
        return sorted(self._scan_data.keys())


# Chrom file support

class Cluster:
    grp_num = 0
    peak_rt = 0
    seedmz = 0
    num_cols = 0
    col_mzs = []
    num_rts = 0
    rts = []
    seed = []
    peaks = []
    spectrum = Spectrum()

    def __init__(self, grp_num=0, peak_rt=0, seedmz=0, num_cols=0, col_mzs=None, num_rts=0, rts=None, seed=None,
                 peaks=None, spectrum=Spectrum()):
        self.grp_num = grp_num
        self.peak_rt = peak_rt
        self.seedmz = seedmz
        self.num_cols = num_cols
        self.col_mzs = list()
        self.num_rts = 0
        self.rts = list()
        self.seed = list()
        self.peaks = list()
        self.spectrum = Spectrum()
        self.average_corr = 0
        self.top_ten_corr = 0
        self.top_ten_intensity_corr = 0

    def max_intensity(self):
        max_intensity = 0
        for peak in self.peaks:
            peak_max = max(peak)
            if peak_max > max_intensity:
                max_intensity = peak_max
        if len(self.seed) > 0:
            max_seed = max(self.seed)
            if max_seed > max_intensity:
                max_intensity = max_seed
        return max_intensity


class MS2FileReader:
    EXTRA_ACCESSIONS = [
        ('MS:1000827', ['value']),  # "isolation window target m/z"
        ('MS:1000828', ['value']),  # "isolation window lower offset"
        ('MS:1000829', ['value']),  # "isolation window upper offset"
        ('MS:1000927', ['value'])   # "ion injection time"
    ]

    def __init__(self, filename=""):
        self.filename = handle_possible_windows_shortcut(filename)
        self.state = "initialized"
        self.file_type = None
        self.header = None
        self.scan_to_index = dict()
        self.iFType = None
        self.iVersion = None
        # mzml-specific
        self.msrun = None

    def load_file(self, filename=None, version=None):
        if filename is not None:
            self.filename = handle_possible_windows_shortcut(filename)
        # set the file type
        self.file_type = self.filename.split('.')[-1]
        self.file_type = self.file_type.lower()
        if self.file_type == "cms2":
            self.read_cms2_file(self.filename, version)
        elif self.file_type == "bms2":
            self.read_bms2_file(self.filename)
        elif self.file_type == "ms2":
            self.read_ms2_file(self.filename)
        elif self.file_type == "mzml":
            self.read_mzml_file(self.filename)
        else:
            raise Exception("MS2FileReader: Unknown file type for file {}".format(self.filename))

    def read_cms2_file(self, filename, version=None):
        self.state = "loading"
        self.fin = open(filename, "rb")
        self.scan_to_index = dict()
        # get the iFtype and iVersion
        self.header = list()
        (self.iFType, self.iVersion,) = struct.unpack('=ii', self.fin.read(8))
        if version is not None:
            self.iVersion = version
        sys.stderr.write("type %d\n" % self.iFType)
        sys.stderr.write("version %d\n" % self.iVersion)
        if self.iVersion > 3:
            sys.stderr.write("Warning: cms2 version %d hasn't been tested with this software.\n" % self.iVersion)
        # skip reading the header
        self.fin.read(16 * 128)
        while True:
            current_index = self.fin.tell()
            spectrum_info = self.read_spec_header()
            if not spectrum_info:
                # reached end of file
                break
            # skip the z states
            for i in range(spectrum_info.numZStates):
                self.fin.read(struct.calcsize('=id'))
            for i in range(spectrum_info.numEZStates):
                self.fin.read(struct.calcsize('=idii'))
            if spectrum_info.scanNumber in self.scan_to_index:
                raise Exception("Duplicate scan number {0} found in cms2 file {1}. Existing index is {2}. Duplicate index is {3}.".format(
                    spectrum_info.scanNumber, self.filename, self.scan_to_index[spectrum_info.scanNumber], current_index
                ))
            self.scan_to_index[spectrum_info.scanNumber] = current_index
            # skip over the spectrum peaks and stuff
            (mz_len, intensity_len,) = struct.unpack('=ii', self.fin.read(8))
            self.fin.read(mz_len + intensity_len)
        # self.fin.close()
        self.state = "loaded"

    def read_spec_header(self):
        buffer = self.fin.read(20)
        if buffer == '':
            return False
        return_info = MSScanInfo()
        return_info.iVersion = self.iVersion
        (return_info.scanNumber, return_info.scanNumber2, return_info.mz, return_info.rTime,) = struct.unpack('=iidf',
                                                                                                              buffer)
        if self.iVersion == 2 or self.iVersion == 3:
            (return_info.bpi, return_info.bpm, return_info.conva, return_info.convb, return_info.tic,
             return_info.iit,) = struct.unpack('=fddddf', self.fin.read(40))
            (return_info.numZStates, return_info.numDataPoints,) = struct.unpack('=ii', self.fin.read(8))
        if self.iVersion == 3:
            (return_info.numZStates, return_info.numEZStates, return_info.numDataPoints,) = struct.unpack('=iii',
                                                                                                          self.fin.read(
                                                                                                              12))
        # if self.iVersion ==3:
        #			(return_info.isolation_width, return_info.isolation_center, return_info.low_mass, return_info.high_mass,) = struct.unpack('=dddd', self.fin.read(32))
        return return_info

    def read_bms2_file(self, filename):
        pass

    def read_ms2_file(self, filename):
        pass

    def read_mzml_file(self, filename):
        self.state = "loading"
        self.scan_to_index = dict()
        self.msrun = pymzml.run.Reader(filename, extraAccessions=MS2FileReader.EXTRA_ACCESSIONS)
        num_scans = self.msrun.getSpectrumCount()
        count = 0
        status_time = time.time()
        for spectrum in self.msrun:
            if time.time() - status_time > 0.5 or count == num_scans:
                status_time = time.time()
                try:
                    sys.stderr.write("\rIndexing scan {0} of {1}           ".format(count, num_scans))
                except ValueError as e:
                    # IPython can fail randomly when writing to stderr too quickly
                    if e.message == "I/O operation on closed file":
                        print("""WARNING: Writing to stderr raised exception. This may be due to a known issue with
                                 IPython/Jupyter. This bug may not be timing dependent after all.""")
                    else:
                        print(e.message)
                        raise

            if spectrum['ms level'] == 2:
                try:
                    # use branch of pymzml
                    #scan = int(spectrum['id_']['scan'])

                    scan = spectrum['id']
                except:
                    scan = spectrum['id']
                if scan in self.scan_to_index:
                    raise Exception(
                        "Duplicate scan {0} found in file {1}. Existing index is {2}. Duplicate index is {3}.".format(
                            scan, self.filename, self.scan_to_index[scan], spectrum['id']
                        ))
                self.scan_to_index[scan] = spectrum['id']
            count += 1
        sys.stderr.write("\n")
        self.state = "loaded"

    def at(self, scan):
        if not self.scan_to_index.has_key(scan):
            sys.stderr.write("Tried to access scan {0} but it is not present in the ms2 file\n".format(scan))
            return False, Spectrum()
        return True, self.get_spectrum_from_index(self.scan_to_index[scan])

    def get_valid_scans(self):
        return self.scan_to_index.keys()

    def get_spectrum_from_index(self, index):
        # TODO this is a good place to use polymorphism
        if self.file_type == "cms2":
            return_spectrum = Spectrum()
            #		self.fin = open(self.filename)
            self.fin.seek(index, os.SEEK_SET)
            scan_info = self.read_spec_header()
            return_spectrum.scan_info = scan_info
            # read in the z states
            for i in range(scan_info.numZStates):
                (z, mz,) = struct.unpack('=id', self.fin.read(struct.calcsize('=id')))
                return_spectrum.z_states.append((z, mz))
            # now read the compressed mz/intensity pairs
            (mz_len, intensity_len,) = struct.unpack('=ii', self.fin.read(8))
            c_mz_val_string = self.fin.read(mz_len)
            c_intensity_val_string = self.fin.read(intensity_len)
            mz_val_string = zlib.decompress(c_mz_val_string)
            intensity_val_string = zlib.decompress(c_intensity_val_string)
            a = array.array('d')
            a.fromstring(mz_val_string)
            mz_vals = a.tolist()
            a = array.array('f')
            a.fromstring(intensity_val_string)
            intensity_vals = a.tolist()
            for i in range(len(mz_vals)):
                return_spectrum.add_peak(mz_vals[i], intensity_vals[i])
            # self.fin.close()
            return return_spectrum
        elif self.file_type == "mzml":
            return_spectrum = Spectrum()
            spectrum = self.msrun[index]
            for mz, i in spectrum.peaks:
                return_spectrum.add_peak(mz, i)
            scan_info = MSScanInfo()
            scan_info.tic = spectrum['total ion current']
            scan_info.iit = spectrum['ion injection time']
            return_spectrum.scan_info = scan_info
            return return_spectrum
        else:
            raise Exception("MS2FileReader: {} file type not supported".format(self.file_type))



class ChromFileReader:
    input_filename = ''
    version = 0
    group_info = dict()  # group_info[grp_num] = (iso-center,iso-width)
    rt_vals = dict()  # rt_vals[grp_num] = [rt_vals]
    chrom_indices = []
    clust_indices = []
    file_obj = None

    def __init__(self, input_filename=None, version=0, group_info=None, rt_vals=None, chrom_indices=None,
                 clust_indices=None, file_obj=None):
        if group_info is None:
            group_info = dict()
        if rt_vals is None:
            rt_vals = dict()
        if chrom_indices is None:
            chrom_indices = list()
        if clust_indices is None:
            clust_indices = list()
        self.filename = input_filename
        self.version = version
        self.group_info = dict()
        self.rt_vals = dict()
        self.chrom_indices = list()
        self.clust_indices = list()
        self.file_obj = None
        self.state = "initialized"
        self.scan_to_cluster_index = dict()
        self.merge_scan_to_grp_num = list()

    def read_version_number(self, fin):
        (ver,) = struct.unpack('i', fin.read(4))
        return ver

    def read_rt_values(self, fin):
        (grp_num, num_vals,) = struct.unpack('ii', fin.read(8))
        rt_vals = []
        for i in range(num_vals):
            (temp_rt,) = struct.unpack('f', fin.read(4))
            rt_vals.append(temp_rt)
        return grp_num, rt_vals

    def read_group_info(self, fin):
        (grp_num, iso_center, iso_width,) = struct.unpack('iff', fin.read(12))
        return grp_num, iso_center, iso_width

    def read_indices(self, filename=input_filename):
        # read in the chromatogram indices
        fin = open(filename, "rb")
        fin.seek(-8, os.SEEK_END)
        (indices_start,) = struct.unpack('l', fin.read(8))
        fin.seek(indices_start, os.SEEK_SET)
        while True:
            (header,) = struct.unpack('i', fin.read(4))
            if header == 1:
                (grp_num, mz, index,) = struct.unpack('ifl', fin.read(16))
                self.chrom_indices[grp_num][mz] = index
            else:
                break
        # read in the cluster indices
        fin.seek(-16, os.SEEK_END)
        (indices_start,) = struct.unpack('l', fin.read(8))
        fin.seek(indices_start, os.SEEK_SET)
        counter = 1
        self.merge_scan_to_group_num = list()
        self.merge_scan_to_group_num.append(-1)
        while True:
            (header,) = struct.unpack('i', fin.read(4))
            if header == 6:
                (grp_num, peak_rt, origin_scan,) = struct.unpack('ifi', fin.read(12))
                (index,) = struct.unpack('l', fin.read(8))
                self.clust_indices[grp_num].append((peak_rt, origin_scan, index, counter))
                self.scan_to_cluster_index[counter] = index
                self.merge_scan_to_group_num.append(grp_num)
            else:
                break
            counter += 1
        fin.close()
        for i in range(len(self.clust_indices)):
            self.clust_indices[i].sort()

    def group_num_from_merge_scan(self, scan):
        return self.merge_scan_to_group_num[scan]

    def load_file(self, filename):
        self.filename = filename
        self.state = "loading"
        # read in the file header information (version #, group info, group rts)
        fin = open(filename, 'rb')
        while True:
            (header,) = struct.unpack('i', fin.read(4))
            if header == 0:
                # version number
                self.version = self.read_version_number(fin)
            elif header == 1:
                # index value
                break
            elif header == 2:
                # rt values for a group
                grp_num, rts = self.read_rt_values(fin)
                self.rt_vals[grp_num] = rts
            elif header == 3:
                # chromatogram
                break
            elif header == 4:
                # group info (iso_center, iso_width)
                grp, cent, wid = self.read_group_info(fin)
                self.group_info[grp] = (cent, wid)
            elif header == 5:
                break
            elif header == 6:
                break
            else:
                sys.exit("Header value unrecognized!")
        for i in range(len(self.group_info.keys())):
            self.chrom_indices.append(dict())
            self.clust_indices.append(list())
        fin.close()
        self.read_indices(filename)
        self.file_obj = open(filename, "rb")
        self.state = "loaded"

    def get_chrom(self, group, mz):
        if self.file_obj is None:
            print("Please load a file using the load_file method before calling this function")
        else:
            file_obj_original_pos = self.file_obj.tell()
            if not self.chrom_indices[group].has_key(mz):
                return -1
            index = self.chrom_indices[group][mz]
            self.file_obj.seek(index, os.SEEK_SET)
            chrom = []
            (header, grp_num, n_mz, num_vals) = struct.unpack('iifi', self.file_obj.read(16))
            if header != 3 or grp_num != group:
                sys.exit("Chrom file corrupted")
            elif n_mz != mz:
                print("Invalid m/z query")
                self.file_obj.seek(file_obj_original_pos, os.SEEK_SET)
                return
            else:
                for i in range(num_vals):
                    (temp_intensity,) = struct.unpack('f', self.file_obj.read(4))
                    chrom.append(temp_intensity)
                self.file_obj.seek(file_obj_original_pos, os.SEEK_SET)
                return self.rt_vals[group], chrom
            self.file_obj.seek(file_obj_original_pos, os.SEEK_SET)
            return

    def read_rts(self, index):
        if self.file_obj is None:
            print("Please load a file using the load_file method before calling this function")
            return
        file_obj_original_pos = self.file_obj.tell()
        self.file_obj.seek(index, os.SEEK_SET)
        (header,) = struct.unpack('i', self.file_obj.read(4))
        if header != 5:
            sys.exit("Chrom file corrupted")
        (grp_num, peak_rt, seedmz, num_cols,) = struct.unpack('iffi', self.file_obj.read(16))
        # read in the column mzs
        for i in range(num_cols):
            self.file_obj.read(4)

        # read in the retention time values
        (num_rts,) = struct.unpack('i', self.file_obj.read(4))
        rts = list()
        for i in range(num_rts):
            (temp_rt,) = struct.unpack('f', self.file_obj.read(4))
            rts.append(temp_rt)
        # self.file_obj.seek(file_obj_original_pos, os.SEEK_SET)
        return rts

    def read_cluster(self, index):
        clust = Cluster()
        if self.file_obj is None:
            print("Please load a file using the load_file method before calling this function")
            return
        file_obj_original_pos = self.file_obj.tell()
        self.file_obj.seek(index, os.SEEK_SET)
        (header,) = struct.unpack('i', self.file_obj.read(4))
        if header != 5:
            sys.exit("Chrom file corrupted")
        (grp_num, peak_rt, seedmz, num_cols,) = struct.unpack('iffi', self.file_obj.read(16))
        clust.grp_num = grp_num
        clust.peak_rt = peak_rt
        clust.seedmz = seedmz
        clust.num_cols = num_cols

        # read in the column mzs
        for i in range(num_cols):
            (temp_mz,) = struct.unpack('f', self.file_obj.read(4))
            clust.col_mzs.append(temp_mz)

        # read in the retention time values
        (num_rts,) = struct.unpack('i', self.file_obj.read(4))
        clust.num_rts = num_rts
        for i in range(num_rts):
            (temp_rt,) = struct.unpack('f', self.file_obj.read(4))
            clust.rts.append(temp_rt)

        # populate the peaks structure
        all_rt_vals = self.rt_vals[grp_num]
        min_rt = min(clust.rts)
        max_rt = max(clust.rts)
        min_rt_index = all_rt_vals.index(min_rt)
        max_rt_index = all_rt_vals.index(max_rt)
        for mz in clust.col_mzs:
            chrom = self.get_chrom(grp_num, mz)
            if chrom == -1:
                continue
            chrom = chrom[1]
            if mz == clust.seedmz:
                clust.seed = chrom[min_rt_index:max_rt_index + 1]
            else:
                clust.peaks.append(chrom[min_rt_index:max_rt_index + 1])

        # if it's there, read in correlation information
        curr_pos = self.file_obj.tell()
        (next_header,) = struct.unpack('i', self.file_obj.read(4))
        self.file_obj.seek(8, os.SEEK_CUR)
        (header2,) = struct.unpack('i', self.file_obj.read(4))
        self.file_obj.seek(8, os.SEEK_CUR)
        (header3,) = struct.unpack('i', self.file_obj.read(4))
        header_list = list()
        header_list.append(next_header)
        header_list.append(header2)
        header_list.append(header3)
        header_list.sort()
        self.file_obj.seek(curr_pos, os.SEEK_SET)
        if header_list == [7, 8, 9]:
            # this file has info on correlation
            for i in range(3):
                (next_header,) = struct.unpack('i', self.file_obj.read(4))
                if next_header == 7:
                    # average correlation
                    (clust.average_corr,) = struct.unpack('=d', self.file_obj.read(8))
                if next_header == 8:
                    # top ten intensity correlations
                    (clust.top_ten_intensity_corr,) = struct.unpack('=d', self.file_obj.read(8))
                if next_header == 9:
                    # top ten correlations
                    (clust.top_ten_corr,) = struct.unpack('=d', self.file_obj.read(8))

        # read in the spectrum
        (num_peaks,) = struct.unpack('i', self.file_obj.read(4))
        for i in range(num_peaks):
            (peak_mz, peak_intensity,) = struct.unpack('df', self.file_obj.read(12))
            clust.spectrum.add_peak(peak_mz, peak_intensity)
        self.file_obj.seek(file_obj_original_pos, os.SEEK_SET)
        return clust

    def get_group_info(self):
        return self.group_info

    def get_cluster_scans(self):
        return self.scan_to_cluster_index.keys()

    def get_cluster_indices(self, grp_num=None):
        indices = list()
        if grp_num is None:
            grp_nums = self.clust_indices.keys()
            for grp in grp_nums:
                grp_indices = self.clust_indices[grp]
                grp_indices = [entry[2] for entry in grp_indices]
                indices.extend(grp_indices)
        else:
            grp_indices = self.clust_indices[grp_num]
            grp_indices = [entry[2] for entry in grp_indices]
            indices.extend(grp_indices)
        indices.sort()
        return indices

    def at(self, scan):
        if not self.scan_to_cluster_index.has_key(scan):
            print("Tried to access a cluster at a non-existent scan!")
            return None
        index = self.scan_to_cluster_index[scan]
        cluster = self.read_cluster(index)
        return cluster

    def rts_at(self, scan):
        if not self.scan_to_cluster_index.has_key(scan):
            print("Tried to access a cluster at a non-existent scan!")
            return None
        index = self.scan_to_cluster_index[scan]
        rts = self.read_rts(index)
        return rts


class IndexReader:
    def __init__(self, filename=""):
        self.filename = filename
        self.merge_to_orig = dict()
        self.orig_to_merge = dict()
        self.state = "initialized"

    def load_file(self, filename=None):
        if filename is None:
            filename = self.filename
        else:
            self.filename = filename
        fin = open(self.filename)
        for line in fin:
            values = line.strip().split('\t')
            merge_scan = int(values[-1])
            orig_scan = int(values[1])
            self.merge_to_orig[merge_scan] = orig_scan
            self.orig_to_merge[orig_scan] = merge_scan
        fin.close()
        self.state = "loaded"

    def merge_at(self, orig_scan):
        if self.orig_to_merge.has_key(orig_scan):
            return self.orig_to_merge[orig_scan]
        else:
            print("Tried to find a merge scan from a nonexistent original scan")

    def orig_at(self, merge_scan):
        if self.merge_to_orig.has_key(merge_scan):
            return self.merge_to_orig[merge_scan]
        else:
            print("Tried to find an orig scan from a nonexistent merge scan")


class PercLine:
    def __init__(self, line=None):
        self.filename = ""
        self.scan = 0
        self.charge = 0
        self.match = 0
        self.score = 0
        self.scoreStr = ""
        self.qvalue = 0
        self.qval_str = ""
        self.pep = 0
        self.pep_str = ""
        self.peptide = ""
        self.proteinIds = list()
        if line is not None:
            self.parse_line(line)

    def parse_line(self, line):
        values = line.strip().split('\t')
        file_line = values[0]
        file_vals = file_line.split('_')
        self.match = int(file_vals[-1])
        self.charge = int(file_vals[-2])
        self.scan = int(file_vals[-3])
        self.filename += file_vals[0]
        for entry in file_vals[1:-3]:
            self.filename += '_' + entry
        self.score = float(values[1])
        self.scoreStr = values[1]
        self.qvalue = float(values[2])
        self.qval_str = values[2]
        self.pep = float(values[3])
        self.pep_str = values[3]
        self.peptide = values[4]
        for entry in values[5:]:
            self.proteinIds.append(entry)
