from lxml import etree as ET


class PeptideSpectrumMatch(object):
    def __init__(self):
        self.psm_id = None
        self.scan = None
        self.charge = None
        self.match = None
        self.svm_score = None
        self.q_value = None
        self.pep = None
        self.exp_mass = None
        self.calc_mass = None
        self.peptide_seq = None
        self.protein_id = None
        self.p_value = None

    def is_valid(self):
        members = [attr for attr in dir(PeptideSpectrumMatch()) if not callable(attr) and not attr.startswith("__")]
        for attr in members:
            if getattr(self, attr) is None:
                return False
        return True


class PercolatorReader:
    """
    Reads percolator version 2.x stdout XML files
    """

    def __init__(self, filename=""):
        self.filename = filename
        self.state = "initialized"
        self.results_by_scan = dict()
        self.psm_iter = None
        self.tag_stack = []
        self.elem_stack = []
        # Use d as default since lxml doesn't support default namespaces
        self.NS = {'d': 'http://per-colator.com/percolator_out/15',
                   'p': 'http://per-colator.com/percolator_out/15'}
        self.PSM_ID_KEY = "{{{}}}psm_id".format(self.NS['p'])

    def load_file(self, filename=None):
        if filename is not None:
            self.filename = filename
        tree = ET.parse(self.filename)
        root = tree.getroot()
        psms_elem = root.find("p:psms", self.NS)
        self.psm_iter = psms_elem.iterfind("d:psm", self.NS)

    def __iter__(self):
        return self

    def next(self):
        # should raise StopIteration if EOF
        psm_elem = self.psm_iter.next()

        file_name = psm_elem.attrib[self.PSM_ID_KEY]
        psm = PeptideSpectrumMatch()
        psm.psm_id = file_name
        psm.scan, psm.charge, psm.match = parse_psm_id(file_name)
        psm.svm_score = float(psm_elem.find('d:svm_score', self.NS).text)
        psm.q_value = float(psm_elem.find('d:q_value', self.NS).text)
        psm.pep = float(psm_elem.find('d:pep', self.NS).text)
        psm.exp_mass = float(psm_elem.find('d:exp_mass', self.NS).text)
        psm.calc_mass = float(psm_elem.find('d:calc_mass', self.NS).text)
        psm.peptide_seq = psm_elem.find('d:peptide_seq', self.NS).attrib['seq']
        psm.protein_id = psm_elem.find('d:protein_id', self.NS).text
        psm.p_value = float(psm_elem.find('d:p_value', self.NS).text)
        return psm


class LegacyPercolatorReader:
    """
    Reads percolator version 1.x stdout files
    """

    def __init__(self, filename=None):
        self.filename = filename
        self.state = "initialized"
        self.results_by_scan = dict()
        self.fin = None

    def load_file(self, filename=None):
        if filename is not None:
            self.filename = filename
        self.fin = open(self.filename)
        first_line = self.fin.readline()
        if first_line[:6] == "enzyme":
            # this is a percolator v 1.15 file
            self.fin.readline()
            self.fin.readline()
            self.fin.readline()
            self.fin.readline()

    def __iter__(self):
        return self

    def next(self):
        # should raise StopIteration if EOF
        line = self.fin.next()

        values = line.strip().split('\t')
        psm = PeptideSpectrumMatch()
        file_name = values[0]
        psm.psm_id = file_name
        psm.scan, psm.charge, psm.match = parse_psm_id(file_name)
        psm.q_value = float(values[2])
        # TODO Add other values if old files really need to be read
        return psm


def parse_psm_id(id):
    psm_id_vals = id.split('_')
    match = int(psm_id_vals[-1])
    charge = int(psm_id_vals[-2])
    scan = int(psm_id_vals[-3])
    return scan, charge, match
