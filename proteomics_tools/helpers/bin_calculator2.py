#!/usr/bin/env python

try:
    from mpl_toolkits.mplot3d import Axes3D
    import matplotlib
    import matplotlib.pyplot as plt
    from matplotlib import cm
    import numpy as np
    from numpy import array
except (RuntimeError, ImportError):
    print("Plotting functionality disabled!")
import random
import sys
from math import sqrt

from .peptide_calculator2 import PeptideMapper

# try:
#     from peptide_calculator2 import PeptideMapper, DiffMod
# except ImportError:
#     print("Could not find peptide_mapper.py")

try:
    from ..file_readers.generic import MS2FileReader
except ImportError:
    print("Could not find generic.py")

from bisect import bisect_left, bisect_right


class DataBinner(object):
    """A class to bin data, using a constant bin width and bin offset.  The bin offset sets the location of the first bin.
    For example, with bin width 1 and offset 0, the bins are [0-1],[1-2],[2-3],[3-4]
    with bin width 1 and offset 0.5, the bins are [0-0.5],[0.5-1.5],[1.5-2.5].  The firstBin is used to transpose all bins
    by firstBin number of bins.  If firstBin is 200, the bin normally present at 200 is mapped to bin 0."""

    def __init__(self, binWidth=1.0, offset=0.0, firstBin=0):
        self._binWidth = binWidth
        self._offset = offset
        self._firstBin = firstBin

    def SetBinWidth(self, binWidth):
        self._binWidth = binWidth

    def GetBinWidth(self):
        return self._binWidth

    def SetBinOffset(self, offset):
        self._offset = offset

    def GetBinOffset(self):
        return self._offset

    def SetFirstBin(self, firstBin):
        self._firstBin = firstBin

    def GetFirstBin(self):
        return self._firstBin

    def GetBinFromValue(self, value):
        bin = int((value - self._offset + self._binWidth) / self._binWidth)
        if self._offset == 0:
            bin = bin - 1
        return bin - self._firstBin

    def GetUpperValueFromBin(self, bin):
        bin += self._firstBin
        if self._offset == 0:
            return bin * self._binWidth + self._binWidth
        else:
            return bin * self._binWidth + self._offset

    def GetLowerValueFromBin(self, bin_index):
        bin_index += self._firstBin
        if self._offset == 0:
            return bin_index * self._binWidth
        else:
            return ((bin_index * self._binWidth) - self._binWidth) + self._offset

    def GetCenterValueFromBin(self, bin_index):
        return self.GetLowerValueFromBin(bin_index) + self._binWidth / 2.0


class BinCalculator:
    def __init__(self):
        self.hist_data = dict()
        self._binner = DataBinner(0.01)
        # removed these, use SetBinWidth and SetBinOffset instead
        #		self.bin_size = 0.01
        #		self.bin_offset = 0
        self.min_mz = 0
        self.max_mz = 2000
        self.pep_calc = PeptideMapper()

    # public
    def SetBinWidth(self, binWidth):
        self._binner.SetBinWidth(binWidth)

    def SetBinOffset(self, binOffset):
        self._binner.SetBinOffset(binOffset)

    def GetBinWidth(self):
        return self._binner.GetBinWidth()

    def GetBinOffset(self):
        return self._binner.GetBinOffset()

    def load_file(self, filename):
        fin = open(filename)
        data = fin.readlines()
        header_info = data[0].strip().split('\t')
        self.hist_data = dict()
        self.SetBinWidth(float(header_info[0][1:]))
        self.SetBinOffset(float(header_info[1]))
        self.min_mz = float(header_info[2])
        self.max_mz = float(header_info[3])
        for line in data[1:]:
            values = line.strip().split('\t')
            (low_mz, high_mz) = values[0].split('-')
            low_mz = float(low_mz)
            high_mz = float(high_mz)
            center_mz = (low_mz + high_mz) / 2.0
            intensity = float(values[1])
            bin = self._binner.GetBinFromValue(center_mz)
            if self.hist_data.has_key(bin):
                self.hist_data[bin] += intensity
            else:
                self.hist_data[bin] = intensity
        fin.close()

    def load_cms2(self, filename, file_out=None, start_rt=0, end_rt=999999, intensityTransform="None"):
        """loads a cms2 file, generates a histogram from observed fragment ion peaks
        If intensityTransform is "SqrtMz", peak intensity is transformed to
        m/z * sqrt(intensity) in each spectrum before being added to the histogram, If intensityTransform is "Sqrt",
        intensities are transformed to sqrt(intensity) in each spectrum prior to being added to the histogram, if
        intensityTransform is "None", no transform is applied.
        the m/z of a +2 fragment ion peak"""
        self.hist_data = dict()

        # generate the correct intensity transformer
        intensityTransform = intensityTransform.upper()
        if intensityTransform == "NONE":
            intensityTransformer = lambda x, mz: x
        elif intensityTransform == "SQRT":
            intensityTransformer = lambda x, mz: sqrt(x)
        elif intensityTransform == "SQRTMZ":
            intensityTransformer = lambda x, mz: mz * sqrt(x)
        else:
            sys.stderr.write("[bin_calculator2::load_cms2_match_seq] %s is an unrecognized type of \n" +
                             "intensity transformer, not applying any transform, valid types are: \n" +
                             "None, sqrt, sqrtmz\n")
            intensityTransformer = lambda x, mz: x

        ms2_reader = MS2FileReader()
        sys.stderr.write("Loading cms2 file %s\n" % filename)
        ms2_reader.load_file(filename)
        valid_scans = ms2_reader.get_valid_scans()
        num_val_scans = len(valid_scans)
        for i, scan in enumerate(valid_scans):
            sys.stderr.write("\rProcessing scan %d of %d     " % (i + 1, num_val_scans))
            (success, spec) = ms2_reader.at(scan)
            if spec.scan_info.rTime < start_rt or spec.scan_info.rTime > end_rt:
                continue
            for peak in spec.peaks:
                mz = peak[0]
                intensity = peak[1]
                transIntensity = intensityTransformer(intensity, mz)
                if 0 <= mz <= 2000:
                    bin = self._binner.GetBinFromValue(mz)
                    #					bin = int(mz/self.bin_size)*self.bin_size
                    if self.hist_data.has_key(bin):
                        self.hist_data[bin] += transIntensity
                    else:
                        self.hist_data[bin] = transIntensity

        minBin = min(self.hist_data.keys())
        maxBin = max(self.hist_data.keys())
        self.min_mz = self._binner.GetLowerValueFromBin(minBin)
        self.max_mz = self._binner.GetUpperValueFromBin(maxBin)
        if file_out is not None:
            self.WriteHistogramFile(file_out)
        sys.stderr.write('\n')

    def WriteHistogramFile(self, fileOut, writeZeros=False):
        fout = open(fileOut, 'w')
        fout.write("#%.20f\t%.20f\t%.4f\t%.4f\n" % (self.GetBinWidth(), self.GetBinOffset(), self.min_mz, self.max_mz))
        bins = self.hist_data.keys()
        if writeZeros:
            minBin = min(bins)
            maxBin = max(bins)
            for bin in range(minBin, maxBin + 1):
                minBinMz = self._binner.GetLowerValueFromBin(bin)
                maxBinMz = self._binner.GetUpperValueFromBin(bin)
                if self.hist_data.has_key(bin):
                    histValue = self.hist_data[bin]
                else:
                    histValue = 0.0
                fout.write("%f-%f\t%f\n" % (minBinMz, maxBinMz, histValue))
        else:
            bins.sort()
            for bin in bins:
                minBinMz = self._binner.GetLowerValueFromBin(bin)
                maxBinMz = self._binner.GetUpperValueFromBin(bin)
                histValue = self.hist_data[bin]
                fout.write("%f-%f\t%f\n" % (minBinMz, maxBinMz, histValue))
        fout.close()

    def load_cms2_match_seq(self, cms2File, sequencesAndCharges, fileOut=None, singleOnly=False,
                            intensityTransform="None", useAvgForDoubleCharge=False):
        """loads a cms2 file as well as a list of peptide,charge tuples
         assumes that cms2 file has scans ordered consecutively 1,2,3...
        use MSReindex to convert scan numbers to this format
        for each spectrum in the cms2 file.  Uses the sequenceAndCharges from the list
        to generate a theoretical spectrum with exact m/z values and intensities matched to those
        from the experimental spectrum in the .cms2 file.  If intensityTransform is "SqrtMz", peak intensity is transformed to
        m/z * sqrt(intensity) in each spectrum before being added to the histogram, If intensityTransform is "Sqrt",
        intensities are transformed to sqrt(intensity) in each spectrum prior to being added to the histogram, if
        intensityTransform is "None", no transform is applied.
        If useAvgForDoubleCharge is true, average amino acid mass values are used instead of monoisotopic when calculating
        the m/z of a +2 fragment ion peak"""
        # clear any previous histogram data
        self._hist_data = dict()
        # the sequences and charges in sequencesAndCharges are ordered in the same order as in the cms2 file
        # for cms2 scan i, look up sequencesAndCharges[i-1] to get the matching sequence and charge

        # now process the cms2
        ms2Reader = MS2FileReader()
        print("Loading cms2 file %s" % cms2File)
        ms2Reader.load_file(cms2File)
        validScans = ms2Reader.get_valid_scans()
        if len(validScans) != len(sequencesAndCharges):
            sys.stderr.write("cms2 file and percolator file do not appear to match!")
            return
        numScans = len(validScans)
        for i, (sequence, charge) in enumerate(sequencesAndCharges):
            sys.stderr.write("Scan %d of %d         \r" % (i, numScans))
            validSequence = True
            while sequence.count('[') > 0 and sequence.count(']') > 0:
                leftInd = sequence.find('[')
                rightInd = sequence.find(']')
                modDescrip = sequence[leftInd - 1:rightInd + 1]
                if modDescrip != "C[+57.00]":
                    # skip modifications besides alkylation of cysteine
                    validSequence = False
                    break
                sequence = sequence[:leftInd] + sequence[rightInd + 1:]

            if not validSequence:
                continue

            scanNum = i + 1
            (success, spec) = ms2Reader.at(scanNum)
            pm = PeptideMapper(spec, sequence=sequence)

            # just look at singly charged b and y ions
            pm.ionTypes = ('b', 'y')
            if singleOnly:
                pm.charges = [1]
            elif charge >= 3:
                # if precursor charge is 3 or more,
                # model +1 and +2 charged fragments
                pm.charges = [1, 2]
            else:
                # if precursor charge is 3 or less,
                # model +1 charged fragments
                pm.charges = [1]

            matches = pm.match_theoretical_to_experimental(average_masses=False,
                                                           use_avg_for_double_charge=useAvgForDoubleCharge)
            # matches is a list of format (theoretical peak, closest experimental peak, epsilon mz(theor - exper))
            # each entry is for a theoretical peak and its closest matching experimental peak

            # generate the correct intensity transformer
            intensityTransform = intensityTransform.upper()
            if intensityTransform == "NONE":
                intensityTransformer = lambda x, mz: x
            elif intensityTransform == "SQRT":
                intensityTransformer = lambda x, mz: sqrt(x)
            elif intensityTransform == "SQRTMZ":
                intensityTransformer = lambda x, mz: mz * sqrt(x)
            else:
                sys.stderr.write("[bin_calculator2::load_cms2_match_seq] %s is an unrecognized type of \n" +
                                 "intensity transformer, not applying any transform, valid types are: \n" +
                                 "None, sqrt, sqrtmz\n")
                intensityTransformer = lambda x, mz: x

            for match in matches:
                theorPeak = match[0]
                experPeak = match[1]
                epsilon = match[2]
                if abs(epsilon) < 0.5:
                    mz = theorPeak.mz
                    intensity = experPeak.intensity
                    intensity = intensityTransformer(intensity, mz)
                    if 0 <= mz <= 2000:
                        bin = self._binner.GetBinFromValue(mz)
                        #						bin = int(mz/self.bin_size)*self.bin_size
                        if self.hist_data.has_key(bin):
                            self.hist_data[bin] += intensity
                        else:
                            self.hist_data[bin] = intensity
        minBin = min(self.hist_data.keys())
        maxBin = max(self.hist_data.keys())
        self.min_mz = self._binner.GetLowerValueFromBin(minBin)
        self.max_mz = self._binner.GetUpperValueFromBin(maxBin)
        if fileOut is not None:
            self.WriteHistogramFile(fileOut)

    def load_cms2_perc(self, cms2File, percFile, fileOut=None, singleOnly=False):
        """loads a cms2 file, as well as a percolator results file (from ./MSDaPlDataExtract)
        for each spectrum in the cms2 file.  Uses the sequence from the percolator results file
        to generate a theoretical spectrum with exact m/z values and intensities matched to those
        from the experimental spectrum in the .cms2 file"""
        # clear any previous histogram data
        self._hist_data = dict()
        # process the percolator file
        fin = open(percFile)
        fin.readline()
        # the sequences here are ordered in the same order as in the cms2 file
        # for cms2 scan i, look up percSequences[i-1] to get the matching percolator sequence
        sequencesAndCharges = list()
        for line in fin:
            values = line.strip().split('\t')
            charge = int(values[3])
            sequence = values[5]
            sequencesAndCharges.append((sequence, charge))

        self.load_cms2_match_seq(cms2File, sequencesAndCharges, fileOut, singleOnly)

    def load_bib_sum(self, filename, file_out=None, bin_size=0.01, bin_offset=0.0, ms2Out=None, diffMods=None,
                     precursorHisto=False):
        """Load a bibliospec summary file generate theoretical spectra (m/z and intensity) for each sequence (weighted
        by number of duplicates observed for the sequence) and add these to the histogram"""
        useDiffMods = True
        if diffMods is None:
            useDiffMods = False

        self.SetBinWidth(bin_size)
        self.SetBinOffset(bin_offset)
        self.hist_data = dict()
        fin = open(filename)
        scanCounter = 1
        if ms2Out is not None:
            ms2fout = open(ms2Out, 'w')
            ms2fout.write("H\tMS2 File Generated by bin_calculator2.py\n")
            ms2fout.write("H\tInput file %s\n" % filename)
        if useDiffMods:
            diffDict = {}
            for dm in diffMods:
                if diffDict.has_key(dm.aminoAcid):
                    diffDict[dm.aminoAcid].append(dm)
                else:
                    diffDict[dm.aminoAcid] = [dm]
        sys.stderr.write("\n")
        counter = 0
        for line in fin:
            counter += 1
            sys.stderr.write("Loading bibliospec summary, on line: %d         \r" % counter)
            if line[0].isdigit():
                values = line.strip().split('\t')
                mz = float(values[2])
                charge = int(values[3])
                duplicates = int(values[5])
                sequence = values[6]
                mods = [None] * len(sequence)
                if useDiffMods:
                    for i, aa in enumerate(sequence):
                        if diffDict.has_key(aa):
                            rand = random.random()
                            random.shuffle(diffDict[aa])
                            for dm in diffDict[aa]:
                                if rand < dm.probability:
                                    mods[i] = dm
                                    break

                ms2Peaks = list()
                precursorMz = self.pep_calc.get_sequence_mz(sequence, charge)
                if useDiffMods:
                    modMass = 0
                    for mod in mods:
                        if mod is not None:
                            modMass += mod.monoMass
                    precursorMz += modMass / float(charge)
                if precursorHisto:
                    bin = self._binner.GetBinFromValue(precursorMz)
                    if self.hist_data.has_key(bin):
                        self.hist_data[bin] += duplicates
                    else:
                        self.hist_data[bin] = duplicates
                else:
                    if ms2Out is not None:
                        if useDiffMods:
                            ms2fout.write("S\t%d\t%d\t%.4f\n" % (scanCounter, scanCounter, precursorMz))
                            ms2fout.write("S\t%d\t%d\t%.4f\n" % (scanCounter, self.pep_calc.get_sequence_mz(sequence, 1) + modMass))
                        else:
                            ms2fout.write("S\t%d\t%d\t%.4f\n" % (
                            scanCounter, scanCounter, self.pep_calc.get_sequence_mz(sequence, charge)))
                            ms2fout.write("Z\t%d\t%.4f\n" % (charge, self.pep_calc.get_sequence_mz(sequence, 1)))

                        # get the fragment sizes
                    frag_m_z = list()
                    #					precursor_lower = mz - 5
                    #					precursor_upper = mz + 5
                    frag_m_z = self.pep_calc.get_masses(sequence, 1, 'y', diff_mods=mods)
                    frag_m_z.extend(self.pep_calc.get_masses(sequence, 1, 'b', diff_mods=mods))
                    #					if charge>=3:
                    #						frag_m_z.extend(self.pep_calc.get_masses(sequence, 2, 'y', average_masses=True, diffMods = mods))
                    #						frag_m_z.extend(self.pep_calc.get_masses(sequence, 2, 'b', average_masses=True, diffMods = mods))
                    for frag in frag_m_z:
                        if 200 <= frag <= 2000:
                            # remove fragments within +/-5m/z of the precursor
                            #							if not (frag > precursor_lower and frag < precursor_upper):
                            bin = self._binner.GetBinFromValue(frag)
                            #							bin = int(frag/self.bin_size)*self.bin_size
                            if self.hist_data.has_key(bin):
                                self.hist_data[bin] += duplicates
                            else:
                                self.hist_data[bin] = duplicates
                            ms2Peaks.append((frag, 1.0))

                        # add a-ions, water loss, and ammonia-loss ions  with 1/5th counter per ion
                        # * is loss of ammonia o is loss of water
                        #					other_ions = self.pep_calc.get_masses(sequence, 1, 'a')
                        #					other_ions =self.pep_calc.get_masses(sequence, 1, 'b*', diffMods = mods)
                        #					other_ions.extend(self.pep_calc.get_masses(sequence, 1, 'y*', diffMods = mods))
                        #					other_ions.extend(self.pep_calc.get_masses(sequence, 1, 'bo', diffMods = mods))
                        #					other_ions.extend(self.pep_calc.get_masses(sequence, 1, 'yo', diffMods = mods))
                        #
                        #					if charge>=3:
                        ##						other_ions.extend(self.pep_calc.get_masses(sequence, 2, 'a'))
                        #						other_ions.extend(self.pep_calc.get_masses(sequence, 2, 'b*', average_masses=True, diffMods = mods))
                        #						other_ions.extend(self.pep_calc.get_masses(sequence, 2, 'y*', average_masses=True, diffMods = mods))
                        #						other_ions.extend(self.pep_calc.get_masses(sequence, 2, 'bo', average_masses=True, diffMods = mods))
                        #						other_ions.extend(self.pep_calc.get_masses(sequence, 2, 'yo', average_masses=True, diffMods = mods))
                        #
                        #					for frag in other_ions:
                        #						if frag>=200 and frag<=2000:
                        ##							if not (frag > precursor_lower and frag < precursor_upper):
                        #							bin = self._binner.GetBinFromValue(frag)
                        ##								bin = int(frag/self.bin_size)*self.bin_size
                        #							if self.hist_data.has_key(bin):
                        #								self.hist_data[bin] += (duplicates * (1/5.0))
                        #							else:
                        #								self.hist_data[bin] = (duplicates * (1/5.0))
                        #								ms2Peaks.append((frag,1.0/5.0))
                    if ms2Out is not None:
                        ms2Peaks.sort()
                        for peak in ms2Peaks:
                            ms2fout.write("%.4f %.4f\n" % peak)
                    scanCounter += 1

        fin.close()
        sys.stderr.write('\n')
        if ms2Out is not None:
            ms2fout.close()
        minBin = min(self.hist_data.keys())
        maxBin = max(self.hist_data.keys())
        self.min_mz = self._binner.GetLowerValueFromBin(minBin)
        self.max_mz = self._binner.GetUpperValueFromBin(maxBin)
        if file_out is not None:
            self.WriteHistogramFile(file_out)

    def get_value_range(self, startBin, endBin):
        binRange = range(startBin, endBin + 1)
        returnVals = list()
        for bin in binRange:
            if self.hist_data.has_key(bin):
                returnVals.append(self.hist_data[bin])
            else:
                returnVals.append(0.0)
        return returnVals

    def plot_mz_histogram(self, userAx=None, lineWidth=2, color=None):
        if userAx is None:
            fig = plt.figure()
            ax = fig.add_subplot(111)
        else:
            ax = userAx
        ax.set_xlabel('m/z')
        minBin = min(self.hist_data.keys())
        maxBin = max(self.hist_data.keys())
        binRange = range(minBin, maxBin + 1)
        mzVals = [self._binner.GetCenterValueFromBin(bin) for bin in binRange]
        intensityVals = list()
        for bin in binRange:
            if self.hist_data.has_key(bin):
                intensityVals.append(self.hist_data[bin])
            else:
                intensityVals.append(0.0)
        if color is None:
            ax.plot(mzVals, intensityVals, linewidth=lineWidth)
        else:
            ax.plot(mzVals, intensityVals, linewidth=lineWidth, color=color)
        if userAx is None:
            plt.show()
        return mzVals, intensityVals

    def plot_bins(self, offset, binSize, userAx=None, lineWidth=2, color=None, binShape=None, shapeMax=250.0):
        if userAx is None:
            fig = plt.figure()
            ax = fig.add_subplot(111)
        else:
            ax = userAx
        # plot the histogram
        self.plot_mz_histogram(ax, lineWidth, color)
        # overlay the bin edges
        db = DataBinner(binSize, offset)
        i = 0
        edgeHeight = ax.get_ylim()[1]
        greyBarRanges = list()
        edges = list()
        while True:
            currentMass = db.GetLowerValueFromBin(i)
            if currentMass > self.min_mz:
                if currentMass > self.max_mz:
                    break
                else:
                    edges.append(currentMass)
                    if i % 2 == 0:
                        greyBarRanges.append((currentMass, db.GetBinWidth()))
            i += 1
        if binShape is None:
            # draw light grey boxes to dilineate the bins
            ax.broken_barh(greyBarRanges, (0, edgeHeight), facecolors='lightgrey')
        else:
            shapeX = binShape[0]
            shapeY = binShape[1]

            # get the exact bin edges by linear interpolation
            rightEdge = binSize / 2.0
            leftEdge = -1.0 * rightEdge
            rightHeight = 0.0
            leftHeight = 0.0
            # left edge
            indexLeft = bisect_left(shapeX, leftEdge)
            if indexLeft > 0:
                x1 = shapeX[indexLeft - 1]
                y1 = shapeY[indexLeft - 1]
                x2 = shapeX[indexLeft]
                y2 = shapeY[indexLeft]
                slope = (y2 - y1) / (x2 - x1)
                leftHeight = y2 - (x2 - leftEdge) * slope

            # rightEdge
            indexRight = bisect_right(shapeX, rightEdge)
            if indexRight < len(shapeX):
                x1 = shapeX[indexRight - 1]
                y1 = shapeY[indexRight - 1]
                x2 = shapeX[indexRight]
                y2 = shapeY[indexRight]
                slope = (y2 - y1) / (x2 - x1)
                rightHeight = y2 - (x2 - rightEdge) * slope

            shapeX = shapeX[indexLeft:indexRight]
            shapeY = shapeY[indexLeft:indexRight]
            shapeX.insert(0, leftEdge)
            shapeY.insert(0, leftHeight)
            shapeX.append(rightEdge)
            shapeY.append(rightHeight)

            norm = shapeMax / float(max(shapeY))
            curveY = [norm * entry for entry in shapeY]
            shapeWidth = shapeX[-1] - shapeX[0]
            for i in range(len(edges) - 1):
                startMz = edges[i]
                endMz = edges[i + 1]
                width = endMz - startMz
                curveX = [width * ((entry - shapeX[0]) / shapeWidth) + startMz for entry in shapeX]
                ax.fill_between(curveX, curveY, 0, facecolor="grey", lw=0)

        if userAx is None:
            plt.show()
        return

    def get_offset_cross_section(self, bin_size, output_file=None, minMz=None, maxMz=None):
        """Returns a list of offsets, and bin overlap intensities for every offset between
        0 and bin_size, and the best offset ([offsets], [bin scores], best_offset)"""
        offsets = range(0, int(bin_size * 10000), 100)
        offsets = [entry / 10000.0 for entry in offsets]
        intensities = [self.get_score(offset, bin_size, minMz, maxMz) for offset in offsets]
        if output_file is not None:
            fout = open(output_file, 'w')
            for i in range(len(offsets)):
                fout.write('%f\t%f\n' % (offsets[i], intensities[i]))
            fout.close()
        min_intensity = min(intensities)
        if min_intensity != 0:
            best_offset = offsets[intensities.index(min_intensity)]
        else:
            # find the intensity at 0 which is furthest away from any non-zero intensities
            extending = False
            track_count = 0
            longest_track = 0
            longest_track_start = 0
            cur_start = 0
            for i in range(len(intensities)):
                if intensities[i] == 0:
                    if not extending:
                        extending = True
                        cur_start = i
                        track_count = 1
                    else:
                        track_count += 1
                if intensities[i] != 0:
                    if extending:
                        if track_count > longest_track:
                            longest_track_start = cur_start
                            longest_track = track_count
                        track_count = 0
                    extending = False
            if longest_track == 1:
                best_offset = offsets[longest_track_start]
            else:
                best_offset = offsets[int(longest_track_start + longest_track / 2.0)]
        print("Best offset: %f %f" % (best_offset, min_intensity))
        return offsets, intensities, best_offset

    def plot_offset_cross_section(self, bin_size, output_file=None, minMz=None, maxMz=None, userAx=None, shift=None,
                                  normalize=False):
        """make a plot of all offsets (0-binsize)  at a given bin size"""
        offsets, intensities, best_offset = self.get_offset_cross_section(bin_size, output_file, minMz, maxMz)
        if shift is not None:
            offsets = [entry + shift for entry in offsets]
        if normalize:
            maxIntensity = max(intensities)
            if maxIntensity != 0:
                intensities = [intensity / maxIntensity for intensity in intensities]
        if userAx is None:
            fig = plt.figure()
            ax = fig.add_subplot(111)
            ax.set_xlabel('offset m/z', fontsize='xx-large')
            ax.set_ylabel('N', fontsize='xx-large')
            ax.set_title('Offset vs. Edge Intensity, @ binsize %s\nBest Offset: %f' % (bin_size, best_offset))
        else:
            ax = userAx

        for label in ax.get_xticklabels() + ax.get_yticklabels():
            label.set_fontsize(20)
        ax.plot(offsets, intensities, lw=3)
        if userAx is None:
            plt.show()

    def get_best_offset(self, bin_size):
        """make a plot of all offsets (0-binsize)  at a given bin size"""
        offsets = range(0, int(bin_size * 10000), 100)
        offsets = [entry / 10000.0 for entry in offsets]
        intensities = [self.get_score(offset, bin_size) for offset in offsets]
        min_intensity = min(intensities)
        if min_intensity != 0:
            best_offset = offsets[intensities.index(min_intensity)]
        else:
            # find the intensity at 0 which is furthest away from any non-zero intensities
            extending = False
            track_count = 0
            longest_track = 0
            longest_track_start = 0
            cur_start = 0
            for i in range(len(intensities)):
                if intensities[i] == 0:
                    if not extending:
                        extending = True
                        cur_start = i
                        longest_track += 1
                    else:
                        longest_track += 1
                if intensities[i] != 0:
                    if extending:
                        if track_count > longest_track:
                            longest_track_start = cur_start
                            longest_track = track_count
                        track_count = 0
                    extending = False
            if longest_track == 1:
                best_offset = offsets[longest_track_start]
            else:
                best_offset = offsets[int(longest_track_start + longest_track / 2.0)]
        return best_offset, min_intensity

    def plot_bin_size_cross_section(self, offset, output_file=None):
        """make a plot of all bin sizes 0.1-1.5 at a given offset"""
        bin_sizes = range(1000, 15000, 1)
        bin_sizes = [entry / 10000.0 for entry in bin_sizes]
        intensities = [self.get_score(offset, bin_size) for bin_size in bin_sizes]
        fig = plt.figure()
        ax = fig.add_subplot(111)
        ax.set_xlabel('bin size')
        ax.set_ylabel('N')
        ax.set_title('Bin size vs. Edge Intensity @ offset %s' % offset)
        ax.plot(bin_sizes, intensities)
        plt.show()

    def plot_space(self, min_bin, max_bin, min_offset, max_offset, output_file=None):
        offsets = range(int(min_offset * 10000), int(max_offset * 10000))
        offsets = [entry / 10000.0 for entry in offsets]
        bins = range(int(min_bin * 10000), int(max_bin * 10000))
        bins = [entry / 10000.0 for entry in bins]
        X, Y = np.meshgrid(offsets, bins)
        Z = list()
        bin_offset_int = list()
        total = len(bins) * len(offsets)
        counter = 1
        for bin in bins:
            bin_vals = list()
            for offset in offsets:
                score = self.get_score(offset, bin)
                bin_offset_int.append((bin, offset, score))
                sys.stdout.write("\r%d/%d       " % (counter, total))
                bin_vals.append(score)
                counter += 1
            Z.append(bin_vals)

        if output_file is not None:
            fout = open(output_file, 'w')
            for entry in bin_offset_int:
                fout.write("%s\t%s\t%s\n" % entry)
            fout.close()
        fig = plt.figure()
        ax = Axes3D(fig)
        ax.set_ylabel('bin size')
        ax.set_xlabel('offset')
        ax.set_zlabel('N')
        Z = array(Z)
        '''make a 3d plot of all bin sizes 0-1.5 and all offsets 0-1.5'''
        ax.plot_surface(X, Y, Z, cmap=cm.jet)
        plt.show()

    # private
    '''
    def get_score(self, offset, bin_size):
        counter = -1
        bins = list()
        while True:
            counter+=1
            curr_mz = offset + (bin_size*counter)
            if curr_mz < self.min_mz:
                continue
            if curr_mz > self.max_mz:
                break
            bins.append(curr_mz)
        bin_intensity_sum = 0
        for bin in bins:
            #find which two values this bin is between
            mz_bin = int(bin/self.bin_size)*self.bin_size
            left_mz = mz_bin
            right_mz = mz_bin + self.bin_size
            left_intensity = 0
            right_intensity = 0
            if self.hist_data.has_key(left_mz):
                left_intensity = self.hist_data[left_mz]
            if self.hist_data.has_key(right_mz):
                right_intensity = self.hist_data[right_mz]
            #interpolate to find the intensity at this bin point
            slope = (right_intensity - left_intensity)/(right_mz-left_mz)
            mz_diff = bin - left_mz
            new_intensity = left_intensity + slope*mz_diff
            bin_intensity_sum += new_intensity
        return bin_intensity_sum
    '''

    def get_score(self, offset, binSize, userMinMz=None, userMaxMz=None):
        counter = -1
        binEdgeMzs = list()
        if userMinMz is None or userMinMz < self.min_mz:
            minMz = self.min_mz
        else:
            minMz = userMinMz
        if userMaxMz is None or userMaxMz > self.max_mz:
            maxMz = self.max_mz
        else:
            maxMz = userMaxMz
        while True:
            counter += 1
            currMz = offset + binSize * counter
            if currMz < minMz:
                continue
            if currMz > maxMz:
                break
            binEdgeMzs.append(currMz)
        binIntensitySum = 0
        # for each bin, do a linear interpolation between this bin and the next closest for this value
        for edgeMz in binEdgeMzs:
            edgeBin = self._binner.GetBinFromValue(edgeMz)
            if edgeMz > self._binner.GetCenterValueFromBin(edgeBin):
                neighborBin = edgeBin + 1
                leftMz = self._binner.GetCenterValueFromBin(edgeBin)
                if self.hist_data.has_key(edgeBin):
                    leftIntensity = self.hist_data[edgeBin]
                else:
                    leftIntensity = 0
                rightMz = self._binner.GetCenterValueFromBin(neighborBin)
                if self.hist_data.has_key(neighborBin):
                    rightIntensity = self.hist_data[neighborBin]
                else:
                    rightIntensity = 0
            else:
                neighborBin = edgeBin - 1
                leftMz = self._binner.GetCenterValueFromBin(neighborBin)
                rightMz = self._binner.GetCenterValueFromBin(edgeBin)
                if self.hist_data.has_key(neighborBin):
                    leftIntensity = self.hist_data[neighborBin]
                else:
                    leftIntensity = 0
                if self.hist_data.has_key(edgeBin):
                    rightIntensity = self.hist_data[edgeBin]
                else:
                    rightIntensity = 0

            slope = (rightIntensity - leftIntensity) / (rightMz - leftMz)
            mzDiff = edgeMz - leftMz
            newIntensity = leftIntensity + slope * mzDiff
            binIntensitySum += newIntensity
        return binIntensitySum

    def get_score_fir(self, offset, binSize):
        # for each bin apply a 1-2-1 Finite Impulse Response Filter to this the bin and its neighboring values
        counter = -1
        binEdgeMzs = list()
        while True:
            counter += 1
            currMz = offset + binSize * counter
            if currMz < self.min_mz:
                continue
            if currMz > self.max_mz:
                break
            binEdgeMzs.append(currMz)
        binIntensitySum = 0
        # for each bin, do a linear interpolation between this bin and the next closest for this value
        for edgeMz in binEdgeMzs:
            edgeBin = self._binner.GetBinFromValue(edgeMz)
            if self.hist_data.has_key(edgeBin):
                binIntensitySum += 2 * self.hist_data[edgeBin]
            if self.hist_data.has_key(edgeBin + 1):
                binIntensitySum += self.hist_data[edgeBin + 1]
            if self.hist_data.has_key(edgeBin - 1):
                binIntensitySum += self.hist_data[edgeBin - 1]
        return binIntensitySum


if __name__ == '__main__':
    bibspec_in = False
    hist_in = False
    offset_in = False
    binsize_in = False
    if len(sys.argv) <= 1:
        print("Usage: bin_calculator.py -i <input bibspec summary file> -h <input histogram> -o <offset> -b <binsize>")
        print("This program takes as input either a bibliospec summary file, or a tab-delimited histogram file of the format: ")
        print("m/z<tab>count")
        print("If a bibliospec summary file is given as input, the peptide sequences are read, fragment massess calculated and a histogram generated")
        print("if a binsize is entered, but no offset, the optimal offset is calculated for the given binsize and returned in the this format: ")
        print("offset<tab>binsize<tab>score")
        print("Where a lower score is better")
        print("if both the binsize and offset are passed as arguments, the score is calculated and returned in the same format")
        sys.exit()
    else:
        for i in range(len(sys.argv)):
            if sys.argv[i] == '-i':
                bibspec_in = True
                bibspec_file = sys.argv[i + 1]
            elif sys.argv[i] == '-h':
                hist_in = True
                hist_file = sys.argv[i + 1]
            elif sys.argv[i] == '-o':
                offset_in = True
                offset = float(sys.argv[i + 1])
            elif sys.argv[i] == '-b':
                binsize_in = True
                binsize = float(sys.argv[i + 1])
    if not (bibspec_in or hist_in):
        print("Please input a histogram file or a bibliospec summary file!")
        print("For help, run the program with no arguments")
        sys.exit()

    if not binsize_in:
        print("Please input a binsize!")
        print("For help, run the program with no arguments")
        sys.exit()

    bc = BinCalculator()
    if bibspec_in:
        bc.load_bib_sum(bibspec_file)
    else:
        bc.load_file(hist_file)

    if offset_in and binsize_in:
        score = bc.get_score(offset, binsize)
        print("%.5f\t%.5f\t%.5f" % (offset, binsize, score))
    elif binsize_in:
        (best_offset, score) = bc.get_best_offset(binsize)
        print("%.5f\t%.5f\t%.5f" % (best_offset, binsize, score))
