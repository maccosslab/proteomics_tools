#!/usr/bin/env python

# Written by Austin Keller <atkeller@u.washington.edu>
# MacCoss Lab
# University of Washington, Dept. of Genome Sciences
# 11/1/2016


def flatten(container):
    if not isinstance(container, (list, tuple)):
        yield container
    else:
        for i in container:
            if isinstance(i, (list, tuple)):
                for j in flatten(i):
                    yield j
            else:
                yield i


import os

if os.name == 'nt':
    import pythoncom
    # noinspection PyUnresolvedReferences
    from win32com.shell import shell, shellcon


    # Wraps Windows .lnk shortcut filenames so that they can be used as normal filenames
    # From pywin32 developer Tim Golden: http://timgolden.me.uk/python/win32_how_do_i/read-a-shortcut.html
    # His site provides general methods of accessing windows-specific functionality
    def shortcut_target(filename):
        link = pythoncom.CoCreateInstance(
            shell.CLSID_ShellLink,
            None,
            pythoncom.CLSCTX_INPROC_SERVER,
            shell.IID_IShellLink
        )
        link.QueryInterface(pythoncom.IID_IPersistFile).Load(filename)
        #
        # GetPath returns the name and a WIN32_FIND_DATA structure
        # which we're ignoring. The parameter indicates whether
        # shortname, UNC or the "raw path" are to be
        # returned. Bizarrely, the docs indicate that the
        # flags can be combined.
        #
        name, _ = link.GetPath(shell.SLGP_UNCPRIORITY)
        return name
else:
    def shortcut_target(filename):
        return filename

import os.path
import os


# Convenience function for wrapping any filename that may or may not be a Windows shortcut
def handle_possible_windows_shortcut(filename):
    if not os.name == 'nt':
        return filename

    # check whether file exists
    if not os.path.exists(filename):
        # try converting to windows shortcut
        if os.path.exists(filename + '.lnk'):
            filename = shortcut_target(filename + '.lnk')
        # continue silently even if filename doesn't exists

    return filename
