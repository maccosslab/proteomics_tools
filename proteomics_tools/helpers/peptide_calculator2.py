# Written by Jarrett Egertson <jegertso@u.washington.edu>
# MacCoss Lab
# University of Washington, Dept. of Genome Sciences
# 07/29/2010

# object to calculate peptide masses, with charge taken into account
# return b ions list, y ion list
# mass mapper, takes epsilon val, spectrum list, b ions, y ions list

# on intialization bisect b ion, y ion list create list of epsilons linked to peaks

# masses from http://www.matrixscience.com/help/aa_help.html
# element masses from www.unimod.org/masses.html


import string
import sys
from copy import deepcopy

from proteomics_tools.file_readers.generic import Spectrum, Sequest_match, Annotation

mono_masses = dict()
avg_masses = dict()

mono_masses['H'] = 1.007825035
avg_masses['H'] = 1.00794

mono_masses['C'] = 12
avg_masses['C'] = 12.0107

mono_masses['O'] = 15.99491463
avg_masses['O'] = 15.9994

mono_masses['N'] = 14.003074
avg_masses['N'] = 14.0067

mono_masses['I'] = 126.904473
avg_masses['I'] = 126.90447

mono_masses['S'] = 31.9720707
avg_masses['S'] = 32.065

mono_masses['P'] = 30.973762
avg_masses['P'] = 30.973761

hydrogen_mass = 1.007825035
proton_mass = 1.00727646677
OH_mass = mono_masses['O'] + mono_masses['H']
H2O_mass = mono_masses['O'] + 2 * (mono_masses['H'])
NH3_mass = mono_masses['N'] + 3 * (mono_masses['H'])
CO_mass = mono_masses['C'] + mono_masses['O']
cam_mass = 2 * mono_masses['C'] + 3 * mono_masses['H'] + mono_masses['O'] + mono_masses[
    'N']  # iodoacetamide reacts with cysteine to form S-carbamidomethyl cysteine

avg_hydrogen_mass = avg_masses['H']
avg_OH_mass = avg_masses['O'] + avg_masses['H']
avg_H2O_mass = avg_masses['O'] + 2 * (avg_masses['H'])
avg_NH3_mass = avg_masses['N'] + 3 * (avg_masses['H'])
avg_CO_mass = avg_masses['C'] + avg_masses['O']
avg_cam_mass = 2 * avg_masses['C'] + 3 * avg_masses['H'] + avg_masses['O'] + avg_masses[
    'N']  # iodoacetamide reacts with cysteine to form S-carbamidomethyl cysteine

mono_residue_masses = dict()
mono_residue_masses['A'] = 71.037114
mono_residue_masses['R'] = 156.101111
mono_residue_masses['N'] = 114.042927
mono_residue_masses['D'] = 115.026943
mono_residue_masses['C'] = 103.009185 + cam_mass
mono_residue_masses['E'] = 129.042593
mono_residue_masses['Q'] = 128.058578
mono_residue_masses['G'] = 57.021464
mono_residue_masses['H'] = 137.058912
mono_residue_masses['I'] = 113.084064
mono_residue_masses['L'] = 113.084064
mono_residue_masses['K'] = 128.094963
mono_residue_masses['M'] = 131.040485
mono_residue_masses['F'] = 147.068414
mono_residue_masses['P'] = 97.052764
mono_residue_masses['S'] = 87.032028
mono_residue_masses['T'] = 101.047679
mono_residue_masses['U'] = 150.95363
mono_residue_masses['W'] = 186.079313
mono_residue_masses['Y'] = 163.06332
mono_residue_masses['V'] = 99.068414

avg_residue_masses = dict()
avg_residue_masses['A'] = 71.0779
avg_residue_masses['R'] = 156.1857
avg_residue_masses['N'] = 114.1026
avg_residue_masses['D'] = 115.0874
avg_residue_masses['C'] = 103.1429 + avg_cam_mass
avg_residue_masses['E'] = 129.114
avg_residue_masses['Q'] = 128.1292
avg_residue_masses['G'] = 57.0513
avg_residue_masses['H'] = 137.1393
avg_residue_masses['I'] = 113.1576
avg_residue_masses['L'] = 113.1576
avg_residue_masses['K'] = 128.1723
avg_residue_masses['M'] = 131.1961
avg_residue_masses['F'] = 147.1739
avg_residue_masses['P'] = 97.1152
avg_residue_masses['S'] = 87.0773
avg_residue_masses['T'] = 101.1039
avg_residue_masses['U'] = 150.0379
avg_residue_masses['W'] = 186.2099
avg_residue_masses['Y'] = 163.1733
avg_residue_masses['V'] = 99.1311


class DiffMod(object):
    """A class containing information about a differential modification"""

    def __init__(self, modName=None, aminoAcid=None, probability=None, composition=None):
        self._modName = modName  # Name of modification
        self._aminoAcid = aminoAcid  # Amino acid modified
        self._probability = probability  # likelihood of occurrence
        if composition is not None:
            self.composition = composition  # list of atoms making up the modification
        else:
            self._monoMass = 0
            self._avgMass = 0

    ##properties##
    @property
    def modName(self):
        return self._modName

    @modName.setter
    def modName(self, value):
        self._modName = value

    @property
    def aminoAcid(self):
        return self._aminoAcid

    @aminoAcid.setter
    def aminoAcid(self, value):
        self._aminoAcid = value

    @property
    def composition(self):
        return deepcopy(self._composition)

    @composition.setter
    def composition(self, value):
        self._composition = deepcopy(value)
        self.UpdateMasses()

    @property
    def monoMass(self):
        return self._monoMass

    @property
    def avgMass(self):
        return self._avgMass

    @property
    def probability(self):
        return self._probability

    @probability.setter
    def probability(self, value):
        self._probability = value

    ##functions##
    def UpdateMasses(self):
        self._monoMass = 0
        self._avgMass = 0
        for atom in self._composition:
            self._monoMass += mono_masses[atom]
            self._avgMass += avg_masses[atom]


class AminoAcid(object):
    def __init__(self, abbreviation, mono_mass, avg_mass, diff_mod=None):
        """
        mono_mass and avg_mass are masses of the amino acid, w/o the diff mod
        """
        self._abbreviation = abbreviation
        self._monoMass = mono_mass
        self._avgMass = avg_mass
        if diff_mod is None:
            self._hasDiffMod = False
            self._diffMod = None
            self._diffModMonoMass = 0
            self._diffModAvgMass = 0
        else:
            self._hasDiffMod = True
            self._diffMod = deepcopy(diff_mod)
            self._diffModMonoMass = diff_mod.mono_mass
            self._diffModAvgMass = diff_mod.avg_mass

    # properties
    @property
    def abbreviation(self):
        return self._abbreviation

    @abbreviation.setter
    def abbreviation(self, value):
        self._abbreviation = value

    @property
    def mono_mass(self):
        return self._monoMass + self._diffModMonoMass

    @property
    def avg_mass(self):
        return self._avgMass + self._diffModAvgMass

    @property
    def diff_mod(self):
        if self._hasDiffMod:
            return deepcopy(self._diffMod)
        else:
            return self._diffMod


def cmp(a, b):
    return (a > b) - (a < b)


class Peak(object):
    def __init__(self, mz, intensity):
        self.mz = mz
        self.intensity = intensity
        self.index = -1

    def __cmp__(self, other):
        return cmp(self.mz, other.mz)


class TheoreticalPeak(Peak):
    def __init__(self, mz, annotation=Annotation(), mass_type=None):
        Peak.__init__(self, mz, 0)
        self.mz = mz
        self.annotation = annotation
        # 'mono' or 'avg'
        self.massType = mass_type


class PeptideMapper:
    def __init__(self, spectrum=None, seq_match=None, charges=None, sequence=None, ion_types=('b', 'y')):
        """

        :param Spectrum spectrum:
        :param Sequest_match seq_match:
        :param charges:
        :param sequence:
        :param tuple ion_types:
        """
        if spectrum is None:
            self.spectrum = Spectrum()
        else:
            self.spectrum = spectrum
        self.sequence = ""
        if seq_match is not None:
            self.sequence = seq_match.sequence
        if sequence is not None:
            self.sequence = sequence
        if charges is None:
            self.charges = [1]
        elif not isinstance(charges, list):
            sys.stderr.write("charges parameter should be a list of integers\n")
            sys.stderr.write("assuming integer input and converting to list\n")
            self.charges = [charges]
        self.ionTypes = ion_types
        self.peak_mz_set = None
        self.annotations = None

    def initialize(self, average_masses=False):
        self.build_peak_mz_set(average_masses)
        # now peak_mz_set is a collection of experimental and theoretical peaks sorted by m/z
        self.annotations = [None for entry in self.peak_mz_set if not isinstance(entry, TheoreticalPeak)]
        # annotations is a list the same length as the number of peaks in the spectrum, each index is the annotation
        # for the peak of that corresponding index
        for i, entry in enumerate(self.peak_mz_set):
            if not isinstance(entry, TheoreticalPeak):
                # this is a peak from the experimental spectrum, figure out the closest theoretical peak
                mz = entry.mz
                theor_1_epsilon = None
                theor_2_epsilon = None
                theor_1 = None
                theor_2 = None
                # search for the closest neighbor backwards
                for j in range(i - 1, 0, -1):
                    entry2 = self.peak_mz_set[j]
                    if isinstance(entry2, TheoreticalPeak):
                        theor_1_mz = entry2.mz
                        theor_1_epsilon = theor_1_mz - mz
                        theor_1 = entry2
                        break
                for j in range(i, len(self.peak_mz_set)):
                    entry2 = self.peak_mz_set[j]
                    if isinstance(entry2, TheoreticalPeak):
                        theor_2_mz = entry2.mz
                        theor_2_epsilon = theor_2_mz - mz
                        theor_2 = entry2
                        break
                if theor_1_epsilon is None and theor_2_epsilon is None:
                    continue
                elif theor_1_epsilon is None:
                    self.annotations[entry.index] = (theor_2, entry, theor_2_epsilon)
                elif theor_2_epsilon is None:
                    self.annotations[entry.index] = (theor_1, entry, theor_1_epsilon)
                elif abs(theor_1_epsilon) < theor_2_epsilon:
                    self.annotations[entry.index] = (theor_1, entry, theor_1_epsilon)
                else:
                    self.annotations[entry.index] = (theor_2, entry, theor_2_epsilon)
                    # now self.annotations is a list (theoretical peak, experimental peak, epsilon (theoretical - experimental))

    def build_peak_mz_set(self, average_masses=False, use_avg_for_double_charge=False, diff_mods=None):
        """
        If use_avg_for_double_charge is True, average masses are used when calculating
        the m/z value of doubly or greater charged fragment ion peaks, otherwise
        whatever was set with the average_masses argument is used.  diff_mods is either None
        or a list of the same length as the sequence being analyzed with either None, or a DiffMod
        instance corresponding to each amino acid in the sequence
        """
        peak_mz_set = []
        for i, entry in enumerate(self.spectrum.peaks):
            peak = Peak(entry[0], entry[1])
            peak.index = i
            peak_mz_set.append(peak)
        num_peaks = len(peak_mz_set)
        for charge in self.charges:
            for ionType in self.ionTypes:
                if charge >= 2 and use_avg_for_double_charge:
                    masses = self.get_masses(self.sequence, charge, ionType, average_masses=True, diff_mods=diff_mods)
                    mass_type = 'avg'
                else:
                    masses = self.get_masses(self.sequence, charge, ionType, average_masses=average_masses,
                                             diff_mods=diff_mods)
                    if average_masses:
                        mass_type = 'avg'
                    else:
                        mass_type = 'mono'
                for i in range(len(masses)):
                    mass = masses[i]
                    annotation = Annotation()
                    annotation.color = "Blue"
                    annotation.label = ionType + str(i + 1)
                    annotation.charge = charge
                    annotation.ion_type = ionType
                    peak_mz_set.append(TheoreticalPeak(mass, annotation, mass_type))

        peak_mz_set.sort()
        self.peak_mz_set = peak_mz_set

    def match_theoretical_to_experimental(self, average_masses=False, epsilon=None, use_avg_for_double_charge=False,
                                          diff_mods=None):
        """
        This is a tacked on function (should write this better later) to look for experimental peaks matching theoretical
        peaks rather than vice-versa.  This means each theoretical peak will occur once in the returned list, with its
        closest matching experimental peak. Returns a list of tuples:
        (theoretical peak, closest experimental peak, epsilon (experimental - theoretical))
        If use_avg_for_double_charge is True, average masses are used when calculating the m/z value
        of doubly or greater charged fragment ion peaks, otherwise, whatever was set
        with the average_masses argument is used
        diff_mods is either None
        or a list of the same length as the sequence being analyzed with either None, or a DiffMod
        instance corresponding to each amino acid in the sequence
        """

        # note there is a lot of overlap with the intialize function, for now this will have to work, recode later if needed
        self.build_peak_mz_set(average_masses, use_avg_for_double_charge, diff_mods=diff_mods)
        return_list = list()
        for i, entry in enumerate(self.peak_mz_set):
            if isinstance(entry, TheoreticalPeak):
                # this is a peak from the theoretical spectrum, figure out the closest experimental peak
                mz = entry.mz
                exper_1_epsilon = None
                exper_2_epsilon = None
                exper_1 = None
                exper_2 = None
                # search for the closest neighbor backwards
                for j in range(i - 1, 0, -1):
                    entry2 = self.peak_mz_set[j]
                    if not isinstance(entry2, TheoreticalPeak):
                        # found an experimental peak
                        exper_1_mz = entry2.mz
                        #						exper_1_epsilon = mz - exper_1_mz
                        exper_1_epsilon = exper_1_mz - mz
                        exper_1 = entry2
                        break
                for j in range(i, len(self.peak_mz_set)):
                    entry2 = self.peak_mz_set[j]
                    if not isinstance(entry2, TheoreticalPeak):
                        # found an experimental peak
                        exper_2_mz = entry2.mz
                        #						exper_2_epsilon = mz - exper_2_mz
                        exper_2_epsilon = exper_2_mz - mz
                        exper_2 = entry2
                        break
                if epsilon is None:
                    # user didn't request any limit on peak distance
                    if exper_1_epsilon is None and exper_2_epsilon is None:
                        continue
                    elif exper_1_epsilon is None:
                        return_list.append((entry, exper_2, exper_2_epsilon))
                    elif exper_2_epsilon is None:
                        return_list.append((entry, exper_1, exper_1_epsilon))
                    elif abs(exper_1_epsilon) < exper_2_epsilon:
                        return_list.append((entry, exper_1, exper_1_epsilon))
                    else:
                        return_list.append((entry, exper_2, exper_2_epsilon))
                else:
                    # user requested a limit on peak distance
                    if exper_1_epsilon is None and exper_2_epsilon is None:
                        continue
                    elif exper_1_epsilon is None:
                        if abs(exper_2_epsilon) <= epsilon:
                            return_list.append((entry, exper_2, exper_2_epsilon))
                    elif exper_2_epsilon is None:
                        if abs(exper_1_epsilon) <= epsilon:
                            return_list.append((entry, exper_1, exper_1_epsilon))
                    elif abs(exper_1_epsilon) < abs(exper_2_epsilon):
                        if abs(exper_1_epsilon) <= epsilon:
                            return_list.append((entry, exper_1, exper_1_epsilon))
                    else:
                        if abs(exper_2_epsilon) <= epsilon:
                            return_list.append((entry, exper_2, exper_2_epsilon))
        return return_list

    def get_annotations(self, epsilon, charge_set=None):
        # return just annotations for each peak
        # if a peak does not have an annotation, it's index is placeholdered with None
        if charge_set is None:
            charge_set = self.charges
        ret_annot = [None] * len(self.spectrum.peaks)
        for i in range(len(self.annotations)):
            entry = self.annotations[i]
            annot = entry[1]
            eps = entry[2]
            if abs(eps) <= epsilon:
                if annot.charge in charge_set:
                    ret_annot[i] = annot
        return ret_annot

    def get_annotations_and_peaks(self, epsilon=None):
        # return the theoretical peak, experimental peak, and annotation for all with mz error <= epsilon
        if epsilon is None:
            return [entry for entry in self.annotations if entry is not None]
        else:
            return [entry for entry in self.annotations if abs(entry[2]) <= epsilon]

    # helper functions
    @staticmethod
    def get_masses(sequence, charge, ion_type, average_masses=False, diff_mods=None):
        """
        Returns a list of m/z values

        :param charge:
        :param ion_type:
        :param average_masses:
        :param sequence:
        :param list diff_mods:
            Can either be None or a list of differential modifications the same length as sequence, containing None if
            there is no differential mod, or the diff mod if there is a diff mod for amino acid at index X
        """

        # Check to see if the peptide has flanking sequence
        use_diff_mods = True
        if sequence.count('.') > 0:
            sequence = sequence.split('.')[1]
        if diff_mods is None:
            use_diff_mods = False
        else:
            deletions = ""
            sequence = string.translate(sequence, None, deletions="#*")
            if len(diff_mods) != len(sequence):
                print(diff_mods)
                print(sequence)
                raise Exception("[PeptideCalculator2::get_masses]: length of diff_mods must = length of sequence\n")

        if average_masses:
            call_attr = 'avg_mass'
        else:
            call_attr = 'mono_mass'
        masses = []
        if len(sequence) > 0:
            sequence = sequence.upper()
            # convert sequence into amino acid masses
            aa_sequence = []
            if use_diff_mods:
                for i, aa_abbrev in enumerate(sequence):
                    aa_sequence.append(
                        AminoAcid(aa_abbrev, mono_residue_masses[aa_abbrev], avg_residue_masses[aa_abbrev], diff_mods[i]))
            else:
                for aa_abbrev in sequence:
                    aa_sequence.append(AminoAcid(aa_abbrev, mono_residue_masses[aa_abbrev], avg_residue_masses[aa_abbrev]))

            seq_list = []
            if ion_type[0] in ['b', 'a']:
                for i in range(1, len(aa_sequence)):
                    seq_list.append(aa_sequence[:i])
            elif ion_type[0] == 'y':
                for i in range(1, len(aa_sequence)):
                    seq_list.append(aa_sequence[i:])

            for frag in seq_list:
                fragM = 0
                for aa in frag:
                    fragM += getattr(aa, call_attr)
                if ion_type[0] == 'y':
                    if average_masses:
                        fragM += avg_OH_mass + avg_hydrogen_mass
                    else:
                        fragM += OH_mass + hydrogen_mass
                elif ion_type[0] == 'a':
                    if average_masses:
                        fragM = fragM - avg_CO_mass
                    else:
                        fragM = fragM - CO_mass
                if len(ion_type) > 1 and ion_type[1] == '*':
                    if average_masses:
                        fragM -= avg_NH3_mass
                    else:
                        fragM -= NH3_mass
                elif len(ion_type) > 1 and ion_type[1] == 'o':
                    if average_masses:
                        fragM -= avg_H2O_mass
                    else:
                        fragM -= H2O_mass
                # now frag1M/frag2M is the non-charged mass
                frag_ch_mass = (fragM + charge * proton_mass) / float(charge)
                masses.append(frag_ch_mass)
        masses.sort()
        return masses

    @staticmethod
    def get_sequence_mz(sequence, charge, average_masses=False, diff_mods=None):
        if average_masses:
            residue_masses = avg_residue_masses
        else:
            residue_masses = mono_residue_masses

        mass = 0
        if sequence.count('.') > 0:
            sequence = sequence.split('.')[1]
        sequence = sequence.upper()
        for aa in sequence:
            mass += residue_masses[aa]
        if diff_mods is not None:
            if average_masses:
                call_attr = 'avg_mass'
            else:
                call_attr = 'mono_mass'
            for dm in diff_mods:
                if dm is not None:
                    mass += getattr(dm, call_attr)
        mass += OH_mass + hydrogen_mass
        if charge != 0:
            mass += proton_mass * charge
            mass /= float(charge)
        return mass

    def get_tryptic_peptides(self, protein_sequence):
        cut_sites = list()
        protein_sequence = protein_sequence.upper()
        for i in range(len(protein_sequence) - 1):
            aa = protein_sequence[i]
            if aa in ['K', 'R']:
                if protein_sequence[i + 1] != 'P':
                    cut_sites.append(i)
        if len(cut_sites) == 0:
            return list()
        tryptic_peptides = list()
        for i in range(len(cut_sites)):
            if i == 0:
                tryptic_peptides.append(protein_sequence[:cut_sites[i] + 1])
            else:
                tryptic_peptides.append(protein_sequence[(cut_sites[i - 1] + 1):cut_sites[i] + 1])
        tryptic_peptides.append(protein_sequence[(cut_sites[-1] + 1):])
        return tryptic_peptides
