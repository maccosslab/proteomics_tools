#!/usr/bin/env python

# Written by Austin Keller <atkeller@u.washington.edu>
# MacCoss Lab
# University of Washington, Dept. of Genome Sciences
# 10/25/2016


import pymzml
from ..helpers.helpers import flatten


EXTRA_ACCESSIONS = [
    ('MS:1000827', ['value']),  # "isolation window target m/z"
    ('MS:1000828', ['value']),  # "isolation window lower offset"
    ('MS:1000829', ['value'])   # "isolation window upper offset"
]


class IsolationWindow:
    def __init__(self, target, lower_offset, upper_offset):
        self.target = target
        self.lower = target - lower_offset
        self.upper = target + upper_offset
        self.upper_offset = upper_offset
        self.lower_offset = lower_offset

    def __eq__(self, other):
        return self.__hash__() == hash(other)

    def __cmp__(self, other):
        return self.__hash__() - hash(other)

    def __hash__(self):
        return int(round(self.target * 10))

    def __str__(self):
        return "{0} {1} {2}".format(self.target, self.lower, self.upper)

    def contains(self, mz):
        return self.lower <= mz <= self.upper


class IsolationScheme:
    def __init__(self):
        self.isolation_windows = set()
        self.cycle = []
        self.num_nonunique_windows = 0
        self.num_nonunique_window_sets = 0

    def cycle_size(self):
        return len(self.cycle)

    def add_windows(self, window_set):
        assert type(window_set) is set

        # Add to the scheme
        new_set = True
        for existing_window_set in self.cycle:
            if existing_window_set == window_set:
                new_set = False
        if not new_set:
            self.num_nonunique_window_sets += 1
        else:
            # add the window set
            self.cycle.append(window_set)
            # reset the count
            self.num_nonunique_window_sets = 0

        # Add to the set of isolation windows, which is just a flattened scheme
        for window in window_set:
            new_window = True
            if window in self.isolation_windows:
                new_window = False
            else:
                self.isolation_windows.add(window)

            if not new_window:
                self.num_nonunique_windows += 1
            else:
                # reset the count
                self.num_nonunique_windows = 0


class IsolationSchemeReader:
    def __init__(self):
        self.scheme_ = IsolationScheme()
        self.isolation_windows_ = []

    def clear(self):
        self.__init__()

    def read(self, infilename):
        """
        Reads an mzml file to find the cyclic isolation scheme used

        :param infilename: Opened mzml file
        :return:
        """
        self.clear()

        msrun = pymzml.run.Reader(infilename, extraAccessions=EXTRA_ACCESSIONS)
        for spectrum in msrun:
            if spectrum["ms level"] == 2:
                isolation_window_set = set()
                isolation_windows = self.pymzml_spectrum_to_isolation_windows(spectrum)
                if len(isolation_windows) == 0:
                    print("WARNING: No precursor info for an MS2 spectrum")
                for w in isolation_windows:
                    isolation_window_set.add(w)
                self.scheme_.add_windows(isolation_window_set)
            if self.scheme_.num_nonunique_windows > self.scheme_.cycle_size() * 2.5:
                break

        # Convert to sorted list
        self.isolation_windows_ = list(self.scheme_.isolation_windows)
        self.isolation_windows_.sort()

    @staticmethod
    def pymzml_spectrum_to_isolation_windows(spectrum):
        """

        :param spectrum:
        :rtype: list[IsolationWindow]
        :return:
        """
        isolation_windows = []
        if "precursors" in spectrum.keys():
            iso_targets = flatten(spectrum["isolation window target m/z"])
            # for target in iso_targets:
            #     print(target)
            iso_uppers = flatten(spectrum["isolation window lower offset"])
            iso_lowers = flatten(spectrum["isolation window upper offset"])
            for t, u, l in zip(iso_targets, iso_uppers, iso_lowers):
                isolation_windows.append(IsolationWindow(t, u, l))
        return isolation_windows

    def get_scheme(self):
        return self.scheme_

    def get_isolation_windows(self):
        return self.isolation_windows_

    def write_scheme(self, outfilename, precision=None, row_format=('lower', 'upper')):
        """

        :param row_format:
        :param precision:
        :param outfilename:
        :return:
        """
        with open(outfilename, 'w') as f:
            for window in self.isolation_windows_:
                for item_name in row_format:
                    item = getattr(window, item_name)
                    if precision is not None:
                        item = round(item, precision)
                    f.write("{0:.12f}".format(item))
                    if item_name == row_format[-1]:
                        f.write("\n")
                    else:
                        f.write("\t")
            f.close()
            print("Isolation scheme written to {}".format(outfilename))