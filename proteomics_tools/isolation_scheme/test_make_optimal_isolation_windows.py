import unittest

from proteomics_tools.isolation_scheme.optimal_isolation_window_generation import *


class TestOptimizeWindowScheme(unittest.TestCase):
    def setUp(self):
        self.optimized_windows_from_skyline = {
            "range": (400.0, 1000.0),
            "width": 24.0,
            "window_boundaries": [
                (400.4319, 424.4428),
                (424.4428, 448.4537),
                (448.4537, 472.4646),
                (472.4646, 496.4756),
                (496.4756, 520.4865),
                (520.4865, 544.4974),
                (544.4974, 568.5083),
                (568.5083, 592.5192),
                (592.5192, 616.5301),
                (616.5301, 640.5410),
                (640.5410, 664.5520),
                (664.5520, 688.5629),
                (688.5629, 712.5738),
                (712.5738, 736.5847),
                (736.5847, 760.5956),
                (760.5956, 784.6065),
                (784.6065, 808.6174),
                (808.6174, 832.6284),
                (832.6284, 856.6393),
                (856.6393, 880.6502),
                (880.6502, 904.6611),
                (904.6611, 928.6720),
                (928.6720, 952.6829),
                (952.6829, 976.6938),
                (976.6938, 1000.7048)]
        }

    def test_window_boundaries(self):
        self.fail("Test not fully implemented")
        return

        data = self.optimized_windows_from_skyline  # short alias
        window_gen = IsolationWindowGenerator(window_width=data["width"],
                                              num_windows=len(data["window_boundaries"]),
                                              first_window=data["range"][0])

        isolation_windows = window_gen.get_isolation_windows()

        for window, (start_mz, end_mz) in zip(isolation_windows, data["window_boundaries"]):
            self.assertAlmostEquals(start_mz, window.lower)
            self.assertAlmostEquals(end_mz, window.upper)


if __name__ == '__main__':
    unittest.main()
