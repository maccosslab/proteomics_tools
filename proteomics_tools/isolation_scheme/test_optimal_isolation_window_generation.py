import unittest

from .optimal_isolation_window_generation import *


class TestOptimalIsolationWindowGeneration(unittest.TestCase):

    def test_isolation_window_generator(self):
        # Known good optimized window boundaries from Skyline
        optimized_windows_from_skyline = {
            "range": (400.0, 1000.0),
            "width": 24.0,
            "window_boundaries": [
                (400.4319, 424.4428),
                (424.4428, 448.4537),
                (448.4537, 472.4646),
                (472.4646, 496.4756),
                (496.4756, 520.4865),
                (520.4865, 544.4974),
                (544.4974, 568.5083),
                (568.5083, 592.5192),
                (592.5192, 616.5301),
                (616.5301, 640.5410),
                (640.5410, 664.5520),
                (664.5520, 688.5629),
                (688.5629, 712.5738),
                (712.5738, 736.5847),
                (736.5847, 760.5956),
                (760.5956, 784.6065),
                (784.6065, 808.6174),
                (808.6174, 832.6284),
                (832.6284, 856.6393),
                (856.6393, 880.6502),
                (880.6502, 904.6611),
                (904.6611, 928.6720),
                (928.6720, 952.6829),
                (952.6829, 976.6938),
                (976.6938, 1000.7048)]
        }

        generator = IsolationWindowGenerator()
        # Generate windows to be tested
        isolationWindows = generator.generate_isolation_windows(windowWidth=optimized_windows_from_skyline["width"],
                                                                start=optimized_windows_from_skyline["range"][0],
                                                                end=optimized_windows_from_skyline["range"][1]
                                                                )

        # Verify windows are equivalent
        for window, (start, end) in zip(isolationWindows, optimized_windows_from_skyline["window_boundaries"]):
            assert np.isclose(start, window['start'])
            assert np.isclose(end, window['end'])


if __name__ == '__main__':
    unittest.main()

