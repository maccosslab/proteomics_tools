from pyteomics import mass
import copy
from collections import namedtuple
import sys
import argparse


class PeptideModificationList:
    comet_params_keys = {
        'A': "add_A_alanine",
        'B': "add_B_user_amino_acid",
        'C': "add_C_cysteine",
        'D': "add_D_aspartic_acid",
        'E': "add_E_glutamic_acid",
        'F': "add_F_phenylalanine",
        'G': "add_G_glycine",
        'H': "add_H_histidine",
        'I': "add_I_isoleucine",
        'J': "add_J_user_amino_acid",
        'K': "add_K_lysine",
        'L': "add_L_leucine",
        'M': "add_M_methionine",
        'N': "add_N_asparagine",
        'O': "add_O_ornithine",
        'P': "add_P_proline",
        'Q': "add_Q_glutamine",
        'R': "add_R_arginine",
        'S': "add_S_serine",
        'T': "add_T_threonine",
        'U': "add_U_user_amino_acid",
        'V': "add_V_valine",
        'W': "add_W_tryptophan",
        'X': "add_X_user_amino_acid",
        'Y': "add_Y_tyrosine",
        'Z': "add_Z_user_amino_acid"
    }

    ReactionComposition = namedtuple('ReactionComposition', ['amino_acid', 'reagent'])

    def __init__(self):
        self.aa_mass_data = copy.deepcopy(mass.nist_mass)
        self.available_reactions = {
            'carbamidomethylation': PeptideModificationList.ReactionComposition(
                amino_acid=mass.Composition(formula='H-1'),
                reagent=mass.Composition(formula='H4NOC2'))
        }
        self.aa_comp = copy.deepcopy(mass.std_aa_comp)
        self.reactions = {}

    def list_modifications(self):
        return self.available_reactions.keys()

    def validate_isotopic_distribution_update(self, update_dict):
        # TODO check that percentages add up to 1.0
        pass

    def update_aa_isotopic_distribution(self, update_dict):
        for element, composition in self.aa_mass_data.iteritems():
            for num_neutrons, percent_composition in composition.iteritems():
                if num_neutrons == 0:
                    # This is the mean, skip for now
                    pass
                else:
                    if element in new_composition:
                        mz, _ = percent_composition
                        if num_neutrons in new_composition[element]:
                            percent_composition = mz, new_composition[element][num_neutrons]
                        else:
                            percent_composition = mz, 0.0
                    self.aa_mass_data[element][num_neutrons] = percent_composition

            # Recalculate the mean
            masses = []
            for num_neutrons, percent_composition in composition.iteritems():
                if num_neutrons == 0:
                    # This is the mean, skip for now
                    pass
                else:
                    mz, ratio = percent_composition
                    masses.append(mz * ratio)

            self.aa_mass_data[element][0] = (sum(masses), 1.0)

    def set_reactions(self, reactions):
        # validate
        for key in reactions:
            if key not in self.aa_mass_data:
                raise Exception("Unknown amino acid {}".format(key))
            for rxn in reactions[key]:
                if rxn.name not in self.available_reactions:
                    raise Exception("Unknown reaction {}".format(rxn.name))
        self.reactions = reactions

    def print_modification_list(self, style='txt'):
        # Apply the new isotopic composition to the amino acids. Then apply the available_reactions assuming that they have a standard
        # isotopic composition
        results = []
        for aa in self.aa_comp:
            if aa == '-OH' or aa == 'H-':
                continue

            aa_mass_from_isotopic_dist = mass.calculate_mass(aa, mass_data=self.aa_mass_data)
            if aa in apply_modifications_to_aa:
                for mod in apply_modifications_to_aa[aa]:
                    rxn = self.available_reactions[mod.name]
                    aa_mass_from_isotopic_dist += mass.calculate_mass(rxn.reagent, mass_data=mod.mass_distribution)
                    aa_mass_from_isotopic_dist += mass.calculate_mass(rxn.amino_acid, mass_data=self.aa_mass_data)
            mass_diff = aa_mass_from_isotopic_dist - mass.calculate_mass(aa)
            results.append((aa, mass_diff))

        if style == 'comet':
            for r in results:
                print("{0}={1}".format(PeptideModificationList.comet_params_keys[r[0]], r[1]))
        elif style == 'xcordia':
            result_string = '-fixed '
            for r in results:
                result_string += "{0}={1},".format(r[0], r[1])
            result_string = result_string.rstrip(',')
            print(result_string)
        elif style == 'txt':
            for r in results:
                print("{0}={1}".format(r[0], r[1]))
        else:
            sys.stderr.write("Unknown style for printing modification list: {}\n".format(style))


parser = argparse.ArgumentParser()
parser.add_argument("-s", "--style", type=str, default='txt',
                    help='<txt|comet|xcordia> Style in which to print the modification list (default=txt).')
args = parser.parse_args()

style = 'txt'
if args.style is not None:
    style = args.style

mod_list = PeptideModificationList()

new_composition = {
    'N': {
        15: 1.0
    }
}

mod_list.update_aa_isotopic_distribution(new_composition)

Reaction = namedtuple('Reaction', ['name', 'mass_distribution'])

apply_modifications_to_aa = {
    'C': [Reaction(name='carbamidomethylation', mass_distribution=mass.nist_mass)]
}

mod_list.set_reactions(apply_modifications_to_aa)
mod_list.print_modification_list(style=style)
