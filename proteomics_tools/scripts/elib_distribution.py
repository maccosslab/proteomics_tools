#!/usr/bin/env python

# Written by Austin Keller <atkeller@u.washington.edu>
# MacCoss Lab
# University of Washington, Dept. of Genome Sciences
# 10/25/2016


# This script reads an elib file and plots the mass distribution of the precursor m/z

from __future__ import print_function

import argparse
import os

from ..visualize.elib_plotter import ElibPlotter

parser = argparse.ArgumentParser()
parser.add_argument("elib")
parser.add_argument("-o", "--output-dir", default=".")
parser.add_argument("-p", "--precursor_mz_bin_width", type=float, default=10.0,
                    help="Bin width in m/z of the precursor histogram")
parser.add_argument("-f", "--fragment_mz_bin_width", type=float, default=10.0,
                    help="Bin width in m/z of the fragment histogram")
parser.add_argument("--precursor_min", type=float, default=-float("inf"),
                    help="Specify a minimum precursor m/z to subset the m/z range")
parser.add_argument("--precursor_max", type=float, default=float("inf"),
                    help="Specify a maximum precursor m/z to subset the m/z range")
args = parser.parse_args()

output_dir = os.path.basename(args.elib) + ".summary"
if args.output_dir is not None:
    if not os.path.exists(args.output_dir):
        raise Exception("Output directory {} does not exist".format(args.output_dir))
    output_dir = os.path.join(os.path.relpath(args.output_dir), output_dir)

if not os.path.exists(output_dir):
    os.mkdir(output_dir)

plotter = ElibPlotter(fragment_bin_width=args.fragment_mz_bin_width,
                      precursor_mz_range=[args.precursor_min, args.precursor_max])
plotter.read(args.elib)
with open(os.path.join(output_dir, "summary.txt"), 'w') as f:
    f.writelines(plotter.summarize_precursor_entries())
plotter.plot_precursor_distribution(mz_bin_size=args.precursor_mz_bin_width,
                                    filename=os.path.join(output_dir, "precursor_distribution.png"))
plotter.plot_ignored_precursor_distribution(mz_bin_size=args.precursor_mz_bin_width,
                                            filename=os.path.join(output_dir, "ignored_precursor_distribution.png"))
plotter.plot_fragment_count_distribution(filename=os.path.join(output_dir, "fragment_count_distribution.png"))
plotter.plot_fragment_intensity_distribution(filename=os.path.join(output_dir, "fragment_intensity_distribution.png"))
