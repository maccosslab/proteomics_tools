#!/usr/bin/env python

# Written by Austin Keller <atkeller@u.washington.edu>
# MacCoss Lab
# University of Washington, Dept. of Genome Sciences
# 02/17/2017


# This script reads two elib files and shows a venn diagram of peptides

from ..visualize.elib_plotter import ElibVennDiagram
import argparse


parser = argparse.ArgumentParser()
parser.add_argument("elibs", nargs='*')
parser.add_argument("--labels", nargs='*')
args = parser.parse_args()

if args.elibs is None:
    raise Exception("At least 2 elib files required")

if args.labels is not None:
    if len(args.labels) != len(args.elibs):
        raise Exception("Number of labels must match the number of elibs")

plotter = ElibVennDiagram()
for i in range(0, len(args.elibs)):
    elib = args.elibs[i]
    if args.labels is not None:
        label = args.labels[i]
    else:
        label = elib
    plotter.add(elib, label)
plotter.plot()
