#!/usr/bin/env python

# Written by Austin Keller <atkeller@u.washington.edu>
# MacCoss Lab
# University of Washington, Dept. of Genome Sciences
# 10/25/2016

import argparse

from ..isolation_scheme.isolation_scheme import IsolationSchemeReader

# Get args
parser = argparse.ArgumentParser()
parser.add_argument("inputfile")
parser.add_argument("-o", "--outputfile")
parser.add_argument("-p", "--precision", type=int)
parser.add_argument("-r", "--row_format",
                    help="""Comma separated list of options describing contents of each output row.
                    Available options include 'lower', 'upper', and 'target'. Default: lower, upper.""")
args = parser.parse_args()

inputfile = args.inputfile
if args.outputfile is not None:
    outputfile = args.outputfile
else:
    outputfile = ''.join(inputfile.split('.')[:-1]) + "_isoscheme.txt"

if args.precision is not None:
    prec = args.precision
else:
    prec = None

row_format = ('lower', 'upper')
if args.row_format is not None:
    row_format = args.row_format.replace(" ", "").split(",")

reader = IsolationSchemeReader()
reader.read(inputfile)
reader.write_scheme(outputfile, precision=prec, row_format=row_format)
