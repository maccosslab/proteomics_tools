#!/usr/bin/env python

# Written by Austin Keller <atkeller@u.washington.edu>
# MacCoss Lab
# University of Washington, Dept. of Genome Sciences
# 05/24/2017


# This script will take two elibs and merge them together, with the option to sample without replacement from one

import argparse
import logging
from shutil import copyfile

from pathlib2 import Path
from sqlalchemy import create_engine

from ..elib_reader.elib_reader import ElibReader
from ..visualize.elib_plotter import ElibVennDiagram

logging.basicConfig(level=logging.INFO)


def ratio(x):
    x = float(x)
    if not 0.0 <= x <= 1.0:
        raise Exception("Ratio must be between 0.0 and 1.0")
    return x


def existing_file(f):
    p = Path(f)
    if not p.is_file():
        raise Exception("File does not exist")
    return f


parser = argparse.ArgumentParser()
parser.add_argument("elib_main", type=existing_file)
parser.add_argument("elib_sample", type=existing_file)
parser.add_argument("out_elib_name")
parser.add_argument("-s", "--sample_fraction", type=ratio, default=0.30,
                    help="The fraction of the peptides to be taken in order from most to least intense")

args = parser.parse_args()
main_file = args.elib_main
sample_file = args.elib_sample
out_elib_filename = args.out_elib_name
sample_fraction = args.sample_fraction


def get_dataframe(elib_filename):
    elib = ElibReader(elib_filename, table='entries')
    entries_df = elib.get_dataframe()
    elib.set_table('peptidequants')
    quant_df = elib.get_dataframe()
    # Merge TotalIntensity into the entries datframe. This works because each PeptideModSeq, PeptideSeq, PrecursorCharge
    # combo is unique
    return entries_df.merge(quant_df[["PeptideModSeq", "PeptideSeq", "PrecursorCharge", "TotalIntensity"]])


# Extract peptides and quant info from two elibs (14N and 15N)
main_df = get_dataframe(main_file)
sample_df = get_dataframe(sample_file)

# Verify no common peptides id'd between two libraries
venn = ElibVennDiagram()
venn.add(main_file)
venn.add(sample_file)
main_set = venn.peptide_sets[0]
sample_set = venn.peptide_sets[1]
intersection = main_set.peptides.intersection(sample_set.peptides)
if len(intersection) > 0:
    raise Exception("Sample set and main set contain {} common peptides".format(len(intersection)))

# Sample 30% of 15N peptides from some upper fraction of intensities (between 7% and 5% dilution of 14N in 15N is when
# unusable RT fit began occurring, while 30% was the minimum to ensure convergence to ideal gaussian shape).
# Could also break the retention time into fractions and sample randomly from within each fraction to ensure coverage of
# full RT span

# Actually, nevermind. Just take the top n% of intensities for simplicity
num_peptides_to_sample = int(round(main_df.shape[0] * sample_fraction))
sample_df.sort_values("TotalIntensity", inplace=True, ascending=False)
new_df = sample_df.head(n=num_peptides_to_sample).drop(['TotalIntensity'], axis=1)

assert new_df.shape[0] == num_peptides_to_sample

# TODO Update the peptidequants table and proteins table accordingly
logging.warning("peptidequants table merge not implemented")
logging.warning("proteins table merge not implemented")

# TODO Verify elib integrity by checking that peptides in each table match as expected
logging.warning("WARNING: Elib integrity check not implemented")

# Write the new elib
copyfile(main_file, out_elib_filename)
engine = create_engine('sqlite:///{}'.format(out_elib_filename))
new_df.to_sql('entries', engine, if_exists='append', index=False)
logging.info("Sampled {} peptides".format(num_peptides_to_sample))
