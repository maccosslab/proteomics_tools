#!/usr/bin/env python

# Written by Austin Keller <atkeller@u.washington.edu>
# MacCoss Lab
# University of Washington, Dept. of Genome Sciences
# 10/25/2016

# This script compares two elibs from a common parent mzml and returns a scan list for peptides found in one but not
# the other elib

import argparse
import os
import sys

import pandas as pd
import pymzml

from ..elib_reader.elib_reader import ElibReader
from ..isolation_scheme.isolation_scheme import IsolationSchemeReader
from ..visualize.elib_plotter import ElibVennDiagram

parser = argparse.ArgumentParser()
parser.add_argument("include_elib", type=os.path.abspath)
parser.add_argument("exclude_elib", type=os.path.abspath)
parser.add_argument("include_mzml", type=os.path.abspath)
parser.add_argument("--outfile", default="scan_list_from_elib_comparison.tsv")
parser.add_argument("--common", action='store_true')
args = parser.parse_args()

include_elib = args.include_elib
exclude_elib = args.exclude_elib
mzml = args.include_mzml

# Test that we can write to the requested file
try:
    fp = open(args.outfile, 'w')
except IOError as e:
    print("Cannot write to file ", args.outfile)
    raise
else:
    fp.close()

elib_comparer = ElibVennDiagram()
# read elib 1
elib_comparer.add(include_elib)

# read elib 2
elib_comparer.add(exclude_elib)

# create list of peptides unique to the include_elib
include_peptide_set = None
exclude_peptide_set = None
for peptide_set in elib_comparer.peptide_sets:
    if peptide_set.source_file == include_elib:
        include_peptide_set = peptide_set.charged_peptides  # type: set
    elif peptide_set.source_file == exclude_elib:
        exclude_peptide_set = peptide_set.charged_peptides  # type: set
assert(include_peptide_set is not None)
assert(exclude_peptide_set is not None)

if args.common:
    peptides_only_in_include = include_peptide_set.intersection(exclude_peptide_set)
else:
    peptides_only_in_include = include_peptide_set.difference(exclude_peptide_set)

# create list of scan times and precursor mz
scan_list = pd.DataFrame()
reader = ElibReader(include_elib, table='entries', selection='RTInSeconds,PrecursorMz,PeptideModSeq,PrecursorCharge')
for row in reader:
    unique_id = ElibVennDiagram.elib_row_to_unique_id(row)
    if unique_id in peptides_only_in_include:
        if len(scan_list) == 0:
            scan_list = pd.DataFrame(row, index=[0])
        else:
            scan_list = scan_list.append(row, ignore_index=True)

# sort by scan times
scan_list.sort_values("RTInSeconds", inplace=True)
scan_list.reset_index(drop=True, inplace=True)

# map scan times and precursor mz to scan indices using mzml
demux_indices = []
original_indices = []
scan_indices = []
spectrum_index = 0
mzml_reader = pymzml.run.Reader(mzml)
num_spectra = mzml_reader.getSpectrumCount()

start_index = 1
end_index = num_spectra - 1
start_time = mzml_reader[start_index]["scan start time"]
end_time = mzml_reader[end_index]["scan start time"]


def binary_search(seq, t, precision):
    low = 0
    high = len(seq) - 1
    while True:
        if high < low:
            return -1
        m = (low + high) // 2
        if seq[m] + precision < t:
            low = m + 1
        elif seq[m] - precision > t:
            high = m - 1
        else:
            assert(abs(seq[m] - t) < precision)
            return m


class RTMzml:
    def __init__(self, run):
        self.run = run

    def __len__(self):
        return self.run.getSpectrumCount()

    def __getitem__(self, item):
        return self.run[item]["scan start time"]

rt_reader = RTMzml(mzml_reader)

for scan_list_index in range(0, len(scan_list["RTInSeconds"])):
    if scan_list_index % 50 == 0:
        sys.stdout.write("\r{0}/{1}".format(scan_list_index, len(scan_list["RTInSeconds"])))

    # Do binary search for retention time
    precision = 0.00001
    find_time = scan_list["RTInSeconds"][scan_list_index] / 60.0
    guess_index = binary_search(rt_reader, find_time, precision)
    if guess_index == -1:
        raise Exception("Could not find retention time {}".format(find_time))
    lower_index_bound = guess_index
    upper_index_bound = guess_index
    # Search down to establish new lower bound of demuxed scans at this retention time
    while True:
        guess_index -= 1
        if not (guess_index >= start_index):
            break
        if not abs(mzml_reader[guess_index]["scan start time"] - find_time) < precision:
            break
        lower_index_bound = guess_index

    # Search up to establish new upper bound of demuxed scans at this retention time
    while True:
        guess_index += 1
        if not (guess_index <= end_index):
            break
        if not abs(mzml_reader[guess_index]["scan start time"] - find_time) < precision:
            break
        upper_index_bound = guess_index

    found_index = False
    precursor_mz = scan_list["PrecursorMz"][scan_list_index]
    for guess_index in range(lower_index_bound, upper_index_bound + 1):
        spectrum = mzml_reader[guess_index]
        isolation_window = IsolationSchemeReader.pymzml_spectrum_to_isolation_windows(spectrum)
        if isolation_window[0].contains(precursor_mz):
            if found_index:
                raise Exception("Found duplicate match")
            found_index = True
            scan_list_index += 1
            demux_indices.append(spectrum['id_']['scan'])
            original_indices.append(spectrum['id_']['originalScan'])
            scan_indices.append(guess_index)

    if not found_index:
        print("Failed to find {}".format(find_time))

sys.stdout.write("\r{0}/{1}".format(len(scan_list["RTInSeconds"]), len(scan_list["RTInSeconds"])))

df_scan_indices = pd.DataFrame({"Index": scan_indices, "Scan": demux_indices, "OriginalScan": original_indices})
df_result = pd.concat([scan_list, df_scan_indices], axis=1)  # type: pd.DataFrame
df_result.to_csv(args.outfile, sep="\t", index=False)