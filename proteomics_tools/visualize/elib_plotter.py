#!/usr/bin/env python

# Written by Austin Keller <atkeller@u.washington.edu>
# MacCoss Lab
# University of Washington, Dept. of Genome Sciences
# 10/25/2016

import argparse
import collections
import struct
import zlib
from collections import namedtuple, Counter
from os.path import basename

import matplotlib.pyplot as plt
import numpy as np
import seaborn as sns
from matplotlib_venn import venn3, venn2

from proteomics_tools.elib_reader import elib_reader


class ElibVennDiagram:
    PeptideSet = namedtuple('PeptideSet', 'name source_file peptides charged_peptides')

    def __init__(self):
        self.peptide_sets = []  # type: list[ElibVennDiagram.PeptideSet]

    def add(self, elib_filename, label=None):
        if label is None:
            label = elib_filename
        reader = elib_reader.ElibReader(elib_filename, table='entries',
                                        selection='PrecursorMz,PeptideModSeq,PrecursorCharge')
        self.peptide_sets.append(
            ElibVennDiagram.PeptideSet(name=label, source_file=elib_filename, peptides=set(), charged_peptides=set()))
        for item in reader:
            # EncyclopeDIA will keep duplicates if they are from different source files
            unique_id = self.elib_row_to_unique_id(item)
            if unique_id not in self.peptide_sets[-1].charged_peptides:
                self.peptide_sets[-1].charged_peptides.add(unique_id)

            # Count unique peptide sequences
            if item['PeptideModSeq'] not in self.peptide_sets[-1].peptides:
                self.peptide_sets[-1].peptides.add(item['PeptideModSeq'])

    @staticmethod
    def elib_row_to_unique_id(row):
        return row['PeptideModSeq'], row['PrecursorCharge']

    def plot(self):
        if len(self.peptide_sets) < 2:
            raise Exception("Need 2 elibs to plot venn diagram, got {}".format(len(self.peptide_sets)))

        if len(self.peptide_sets) > 3:
            raise Exception(
                "Venn diagram of more than 3 sets is not supported. Attempted {}".format(len(self.peptide_sets)))

        peptide_sets = []
        charge_sets = []
        set_labels = []
        for item in self.peptide_sets:
            set_labels.append(item.name)
            peptide_sets.append(item.peptides)
            charge_sets.append(item.charged_peptides)

        # TODO Make obnoxiously long filenames into unique, short labels

        self.plot_sets(peptide_sets, set_labels=set_labels, title="Unique Peptide Sequences")
        self.plot_sets(charge_sets, set_labels=set_labels, title="Unique Peptide Charge and Sequences")

    @staticmethod
    def plot_sets(set_list, set_labels=None, title=""):
        """

        :param list[set] set_list:
        :param list[string] set_labels:
        :param string title:
        :return:
        """

        if len(set_list) == 2:
            venn_func = venn2
        elif len(set_list) == 3:
            venn_func = venn3
        else:
            raise Exception(
                "Cannot plot venn diagram of {} sets. Only 2 and 3 sets are supported.".format(len(set_list)))

        if set_labels is not None:
            venn = venn_func(set_list, set_labels)
        else:
            venn = venn_func(set_list)

        if title:
            plt.title(title)
        plt.show()

    def simplify_filenames(self, filenames):
        """
        Removes redundant info in a set of filenames while maintaining uniqueness
        :param list[string] filenames:
        :return:
        """

        if len(filenames) < 2:
            # TODO trim off front to some arbitrary length
            return filenames

        # Separate by file extension
        file_extensions = set()  # set[string]
        for name in filenames:
            file_extensions.add(name.split('.')[-1])

        filenames_by_ext = {}
        for ext in file_extensions:
            filenames_by_ext[ext] = []
            for name in filenames:
                if not name.endswith(ext):
                    continue
                filenames_by_ext[ext].append(basename(name))

        for ext, basenames in filenames_by_ext:
            # TODO cluster basenames by similarity using SequenceMatcher.ratio() as similarity metric
            pass

    def write_peptide_list(self):
        pass


class Memoize:
    """
    Decorator that caches computed property values
    """

    def __init__(self, fn):
        self.fn = fn
        self.memo = None

    def __call__(self):
        if self.memo is None:
            self.memo = self.fn()
        return self.memo


def memoize(f):
    memoize.memo = None

    def helper(*args, **kwargs):
        if memoize.memo is None:
            memoize.memo = f(*args, **kwargs)
        return memoize.memo

    return helper


def func_once(func):
    """A decorator that runs a function only once."""

    def decorated(*args, **kwargs):
        try:
            return decorated._once_result
        except AttributeError:
            decorated._once_result = func(*args, **kwargs)
            return decorated._once_result

    return decorated


class ElibPlotter:
    def __init__(self, fragment_bin_width=10.0, precursor_mz_range=None):
        self.precursor_mzs = None
        self.ignored_mzs = []
        self.peptide_mod_seq_set = set()
        self.peptide_mod_seq_charge_set = set()
        self.fragment_counts = Counter()
        self.fragment_intensities = {}
        self.fragment_bin_width = fragment_bin_width
        if precursor_mz_range is None:
            precursor_mz_range = [-float("inf"), float("inf")]
        self.precursor_mz_range = precursor_mz_range

    def read(self, filename):
        reader = elib_reader.ElibReader(filename, table='entries',
                                        selection='PrecursorMz,PeptideModSeq,PrecursorCharge,MassEncodedLength,MassArray,IntensityEncodedLength,IntensityArray')
        self.precursor_mzs = []
        self.ignored_mzs = []
        self.peptide_mod_seq_set = set()
        self.peptide_mod_seq_charge_set = set()
        self.fragment_counts = Counter()
        self.fragment_intensities = {}

        for item in reader:
            if not self.precursor_mz_range[0] <= item['PrecursorMz'] <= self.precursor_mz_range[1]:
                continue

            self._add_precursor_entry(item['PeptideModSeq'], item['PrecursorCharge'], item['PrecursorMz'])

            # Decompress mass and intensity blobs
            masses = zlib.decompress(item['MassArray'])
            intensities = zlib.decompress(item['IntensityArray'])

            mass_type = 'd'
            mass_format_string = '>' + (mass_type * (int(item['MassEncodedLength']) // struct.calcsize(mass_type)))
            mass_data = struct.unpack(mass_format_string, masses)

            intensity_type = 'f'
            intensity_format_string = '>' + (
                        intensity_type * (int(item['IntensityEncodedLength']) // struct.calcsize(intensity_type)))
            intensity_data = struct.unpack(intensity_format_string, intensities)

            self._add_fragment_entry(mass_data, intensity_data)

        self.precursor_mzs.sort()
        self.ignored_mzs.sort()

    def summarize_precursor_entries(self):
        yield "Unique peptide-charge pairs: {}\n".format(len(self.precursor_mzs))
        yield "Unique peptide sequences: {}\n".format(len(self.peptide_mod_seq_set))
        yield "Ignored: {}\n".format(len(self.ignored_mzs))

    def _add_precursor_entry(self, peptide_mod_seq, precursor_charge, precursor_mz):
        # EncyclopeDIA will keep duplicates if they are from different source files
        unique_id = (peptide_mod_seq, precursor_charge)
        if unique_id not in self.peptide_mod_seq_charge_set:
            self.peptide_mod_seq_charge_set.add(unique_id)
            self.precursor_mzs.append(precursor_mz)
        else:
            # add to ignored list for debugging
            self.ignored_mzs.append(precursor_mz)

        # Count unique peptide sequences
        if peptide_mod_seq not in self.peptide_mod_seq_set:
            self.peptide_mod_seq_set.add(peptide_mod_seq)

    def _add_fragment_entry(self, masses, intensities):
        for mz, i in zip(masses, intensities):
            # hash the mz
            hashed_mz = round(round(mz / self.fragment_bin_width) * self.fragment_bin_width)
            self.fragment_counts[hashed_mz] += 1
            if hashed_mz not in self.fragment_intensities.keys():
                self.fragment_intensities[hashed_mz] = 0
            self.fragment_intensities[hashed_mz] += i

    def plot_precursor_distribution(self,
                                    mz_bin_size=10.0,
                                    title="Precursor m/z Distribution from Encyclopedia Ids",
                                    filename=None):
        sns.set(color_codes=True, font_scale=1.1)
        nbins = int(round((max(self.precursor_mzs) - min(self.precursor_mzs)) / mz_bin_size))
        sns.distplot(self.precursor_mzs, bins=nbins, kde=False)
        plt.xlabel("Precursor m/z")
        plt.ylabel("Frequency")
        plt.title(title)
        if filename is not None:
            plt.savefig(filename)
        else:
            plt.show()

    def plot_ignored_precursor_distribution(self,
                                            mz_bin_size=10.0,
                                            title="Precursor m/z Distribution from Encyclopedia Ids",
                                            filename=None):
        sns.set(color_codes=True, font_scale=1.1)
        if len(self.ignored_mzs) != 0:
            counter = collections.Counter(self.ignored_mzs)
            plt.scatter(counter.keys(), counter.values())
            plt.xlabel("Precursor m/z")
            plt.ylabel("Frequency")
            plt.title(title)
            if filename is not None:
                plt.savefig(filename)
            else:
                plt.show()

    @property
    @func_once
    def fragment_mzs(self):
        fragment_mzs = self.fragment_counts.keys()
        fragment_mzs.sort()
        fragment_mzs = np.array(fragment_mzs)
        return fragment_mzs

    @property
    @func_once
    def centered_fragment_mzs(self):
        return self.fragment_mzs - self.fragment_bin_width / 2.0

    @property
    @func_once
    def fragment_mz_counts(self):
        return [self.fragment_counts[mz] for mz in self.fragment_mzs]

    @property
    @func_once
    def fragment_mz_intensities(self):
        return [self.fragment_intensities[mz] for mz in self.fragment_mzs]

    def plot_fragment_count_distribution(self,
                                         title="Fragment m/z Distribution from Encyclopedia Ids",
                                         filename=None):
        sns.set(color_codes=True, font_scale=1.1)
        plt.bar(self.centered_fragment_mzs, self.fragment_mz_counts, width=self.fragment_bin_width)
        plt.xlabel("Fragment m/z")
        plt.ylabel("Frequency (counts)")
        plt.title(title)
        if filename is not None:
            plt.savefig(filename)
        else:
            plt.show()

    def plot_fragment_intensity_distribution(self,
                                             title="Fragment m/z Distribution from Encyclopedia Ids",
                                             filename=None):
        sns.set(color_codes=True, font_scale=1.1)
        plt.bar(self.centered_fragment_mzs, self.fragment_mz_intensities, width=self.fragment_bin_width)
        plt.xlabel("Fragment m/z")
        plt.ylabel("Frequency (intensity-weighted)")
        plt.title(title)
        if filename is not None:
            plt.savefig(filename)
        else:
            plt.show()


if __name__ == '__main__':
    """
    This script reads an elib file and plots the mass distribution of the precursor m/z
    """
    # Get args
    parser = argparse.ArgumentParser()
    parser.add_argument("elib")
    args = parser.parse_args()

    inputfile = args.elib

    plotter = ElibPlotter()
    plotter.read(inputfile)
    plotter.plot_precursor_distribution()
    plotter.plot_fragment_distribution()
