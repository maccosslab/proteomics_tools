#!/usr/bin/env python

# Written by Austin Keller <atkeller@u.washington.edu>
# MacCoss Lab
# University of Washington, Dept. of Genome Sciences
# 10/31/2016

import pymzml
import argparse
import copy
import pandas as pd
from ..isolation_scheme.isolation_scheme import IsolationSchemeReader, IsolationWindow
from ..helpers.helpers import flatten

EXTRA_MS1_ACCESSIONS = [

]
EXTRA_MS2_ACCESSIONS = [
    ('MS:1000827', ['value']),  # "isolation window target m/z"
    ('MS:1000828', ['value']),  # "isolation window lower offset"
    ('MS:1000829', ['value']),  # "isolation window upper offset"
    ('MS:1000927', ['value']),  # "ion injection time"
    ("MS:1000501", ['value']),  # "scan window lower limit"
    ("MS:1000500", ['value'])  # "scan window upper limit"

]

EXTRA_ACCESSIONS = EXTRA_MS1_ACCESSIONS + EXTRA_MS2_ACCESSIONS


class PymzmlSpectrumWrapper(object):
    """
    A wrapper class that supports accessing cvparams in scanList. Currently, pymzml will create a nested list when
    accessing cvparams that occur multiple times within the same spectrum. So for spectra with multiple scans this
    creates a nested list that has to be flattened before parsing. This class simply flattens any arrays returned from
    an accessor. Pymzml probably should natively support this operation but this provides an acceptable temporary
    workaround.
    """
    def __init__(self, spectrum):
        self._spectrum = copy.deepcopy(spectrum)
        self._to_flatten = [
            "isolation window target m/z",
            "isolation window lower offset",
            "isolation window upper offset",
            "scan start time",
            "ion injection time",
            "scan window lower limit",
            "scan window upper limit"
        ]

    def __getitem__(self, item):
        orig_item = self._spectrum.__getitem__(item)
        if item in self._to_flatten:
            if isinstance(orig_item, (list, tuple)):
                result = list(flatten(orig_item))
            else:
                result = orig_item
            return result
        else:
            return orig_item

    def __getattr__(self, attr):
        orig_attr = self._spectrum.__getattribute__(attr)
        if callable(orig_attr):
            def hooked(*args, **kwargs):
                result = orig_attr(*args, **kwargs)
                # prevent wrapped_class from becoming unwrapped
                if result == self._spectrum:
                    return self
                return result

            return hooked
        else:
            return orig_attr

    def get_isolation_windows(self):
        iso_targets = self["isolation window target m/z"]
        iso_uppers = self["isolation window lower offset"]
        iso_lowers = self["isolation window upper offset"]
        if not isinstance(iso_targets, (list, tuple)):
            iso_targets = [iso_targets]
            iso_uppers = [iso_uppers]
            iso_lowers = [iso_lowers]
        for t, u, l in zip(iso_targets, iso_uppers, iso_lowers):
            yield IsolationWindow(t, u, l)


class CycleReader(object):
    def __init__(self, infilename, verbose=False):
        self.filename = infilename
        self._verbose = verbose

        # Read the isolation scheme
        iso_reader = IsolationSchemeReader()
        iso_reader.read(infilename)
        self.scheme = iso_reader.get_scheme()

        #DEBUG
        #print("Got scheme")
        #print("Cycle size: {}".format(self.scheme.cycle_size()))

        # Open mzml for reading cycles
        self.msrun = pymzml.run.Reader(self.filename, extraAccessions=EXTRA_ACCESSIONS)

    def __iter__(self):
        cycle = {"ms2": []}
        i = 1
        status_i = 0
        for raw_spec in self.msrun:
            if self._verbose and i >= status_i:
                status_i = i + 100
                percent = round(100.0 * float(i) / self.msrun.info['spectrum_count'], 2)
                sys.stdout.write('\rReading index {0} of {1}: {2}%         '.format(i - 1, self.msrun.info['spectrum_count'], percent))
                sys.stdout.flush()

            # get cycle
            spectrum = PymzmlSpectrumWrapper(raw_spec)
            if spectrum["ms level"] == 1:
                cycle["ms1"] = spectrum
            elif spectrum["ms level"] == 2:
                cycle["ms2"].append(spectrum)
            else:
                sys.stderr.write("Warning: Unhandled ms level: {}\n".format(spectrum["ms level"]))
            if len(cycle["ms2"]) >= self.scheme.cycle_size():

                #DEBUG
                #print("Filled cycle at {}".format(j))
                self.validate_cycle(cycle)

                # Return cycle
                yield cycle

                # Clear cycle container
                cycle = {"ms2": []}
            i += 1
            # DEBUG
            # if i > 1000:
            #     break
        sys.stdout.write('Done reading')
        sys.stdout.flush()

    def validate_cycle(self, cycle):
        # Verify that all windows are contained in the cycle
        windows_seen = set()

        #DEBUG
        # print("Begin Validation")
        # print("Ms level 1 is {}".format(cycle["ms1"]["ms level"]))

        for spectrum in cycle["ms2"]:
            iso_windows = [x for x in spectrum.get_isolation_windows()]
            for w in iso_windows:
                windows_seen.add(w)

        expected_windows = set(self.scheme.isolation_windows)
        if not windows_seen == expected_windows:
            sys.stderr.write("\nWarning: Saw {0} windows, expected {1}\n".format(len(windows_seen), len(expected_windows)))
            sys.stderr.write("Missing windows:\n")
            for w in expected_windows - windows_seen:
                sys.stderr.write('{}\n'.format(w.target))
            return False
        else:
            #DEBUG
            # print("Verified cycle of {} windows".format(len(windows_seen)))
            return True


class FillTimeReader(object):

    def read(self, infilename):
        """
        Reads meta-data from mzml for visualization of fill time

        :param infilename:
        :return:
        """
        count = 0
        charge_list = []
        cycle_reader = CycleReader(infilename, verbose=True)
        for cycle in cycle_reader:
            ms1spectrum = cycle["ms1"]  # type: pymzml.spec.Spectrum
            for ms2spectrum in cycle["ms2"]:
                fill_times = ms2spectrum["ion injection time"]
                if not isinstance(fill_times, list):
                    fill_times = [fill_times]
                for window, fill_time in zip(ms2spectrum.get_isolation_windows(), fill_times):
                    # subset spectrum by window
                    intensities = ms1spectrum.i
                    mzs = ms2spectrum.mz
                    window_intensities = [i for i in self.filter_by_window(intensities, mzs, window)]

                    # sum charge
                    total_charge = sum(window_intensities) * fill_time

                    # Add charge to list with window
                    row = (window.target, window.lower, window.upper, fill_time, total_charge)
                    if len(row) != 5:
                        sys.stderr.write("Skipping row\n")
                        continue
                    for elt in row:
                        if elt is None:
                            sys.stderr.write("Skipping row\n")
                            continue
                    charge_list.append(row)
            count += 1
            # if count >= 100:
            #     break
            # if count % 10 == 0:
            #     print("{}".format(count))
        columns = ['target', 'lower', 'upper', 'fill_time', 'total_charge']
        return pd.DataFrame(charge_list, columns=columns)

    def filter_by_window(self, sequence, mzs, window):
        """

        :param mzs:
        :param sequence:
        :param IsolationWindow window:
        :return:
        """
        for mz, i in zip(mzs, sequence):
            if window.lower <= mz <= window.upper:
                yield i


if __name__ == "__main__":
    import sys
    # Get args
    print(sys.argv)
    parser = argparse.ArgumentParser()
    parser.add_argument("inputfile")
    args = parser.parse_args()

    inputfile = args.inputfile

    reader = FillTimeReader()
    result = reader.read(inputfile)
    print(result)