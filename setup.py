from setuptools import setup, find_packages

with open('README.md') as f:
    readme = f.read()

with open('LICENSE.md') as f:
    project_license = f.read()

setup(
    name='Proteomics Tools',
    version='0.2.1',
    description='Set of scripts for analyzing and visualizing proteomics data',
    long_description=readme,
    author='Austin Keller',
    author_email='atkeller@uw.edu',
    license=project_license,
    packages=find_packages(exclude=('tests', 'docs')),
    install_requires=[
        "matplotlib",
        "numpy",
        "pandas",
        "pymzml",
        "pyteomics",
        "seaborn",
        "matplotlib_venn", 'IPython', 'sqlalchemy', 'lxml', 'pathlib2'
    ]
)
